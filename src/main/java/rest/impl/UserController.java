package rest.impl;

import dto.UserDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import rest.api.UserEndpoint;
import service.api.UserService;

import java.util.List;

@RestController
public class UserController extends BaseController implements UserEndpoint {
    @Autowired
    private UserService userService;

    @Override
    public ResponseEntity<List<UserDto>> getAllUsers() {
        try {
            List<UserDto> dtos = userService.getAllUsers();
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<UserDto>> getUsersByName(@PathVariable("name") String name) {
        try {
            List<UserDto> dtos = userService.getUsersByName(name);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity createUser(@RequestBody UserDto userDto) {
        try {
            final UserDto dto = userService.createUser(userDto);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto) {
        try {
            final UserDto dto = userService.updateUser(userDto);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity deleteUser(@RequestParam("userMail") String email) {
        try {
            userService.deleteUserByEmail(email);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<UserDto> getCurrentUser() {
        try {
            UserDto dto = userService.getCurrentUser();
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<Boolean> isCurrentUserInRole(@PathVariable("role") String role) {
        try {
            return ResponseEntity.ok(userService.isCurrentUserInRole(role));
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }
}

