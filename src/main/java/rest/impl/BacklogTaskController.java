package rest.impl;

import dto.BacklogTaskDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import rest.api.BacklogTaskEndpoint;
import service.api.BacklogTaskService;

import java.util.List;

@RestController
public class BacklogTaskController extends BaseController implements BacklogTaskEndpoint {
    @Autowired
    private BacklogTaskService backlogTaskService;


    @Override
    public ResponseEntity<BacklogTaskDto> createTask(@RequestBody BacklogTaskDto task) {
        try {
            BacklogTaskDto dto = backlogTaskService.createTask(task);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<BacklogTaskDto> updateTask(@RequestBody BacklogTaskDto task,@PathVariable("boardKey") String boardKey) {
        try {
            BacklogTaskDto dto = backlogTaskService.updateTask(task, boardKey);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity removeTaskFromBacklog(@PathVariable("taskKey") String taskKey) {
        try {
            backlogTaskService.removeTaskFromBacklog(taskKey);
            return ResponseEntity.ok().build();
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }


    @Override
    public ResponseEntity<List<BacklogTaskDto>> getAllBacklogTasks(@PathVariable("backlogKey") String backlogKey) {
        try {
            List<BacklogTaskDto> dtos = backlogTaskService.getAllBacklogTasks(backlogKey);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }


    @Override
    public ResponseEntity<BacklogTaskDto> addTaskFromSprintToBacklog(@PathVariable("sprintKey") String sprintKey, @PathVariable("taskKey") String taskKey) {
        try {
            BacklogTaskDto backlogTaskDto = backlogTaskService.addTaskToBacklogFromSprint(sprintKey, taskKey);
            return ResponseEntity.ok(backlogTaskDto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<BacklogTaskDto>> getBacklogByTitleContaining(@PathVariable("title") String title) {
        try {
            List<BacklogTaskDto> dtos = backlogTaskService.getBacklogByTitleContaining(title);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

}
