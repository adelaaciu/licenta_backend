package rest.impl;

import dto.CommentDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.api.CommentEndpoint;
import service.api.CommentService;

import java.util.List;

@RestController
public class CommentController extends BaseController implements CommentEndpoint {
    @Autowired
    private CommentService commentService;

    @Override
    public ResponseEntity<CommentDto> addComment(@RequestBody CommentDto comment) {
        try {
            CommentDto dto = commentService.addComment(comment);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<CommentDto>> getComments(@PathVariable("taskKey") String taskKey) {
        try {
            List<CommentDto> dtos = commentService.getComments(taskKey);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }
}
