package rest.impl;

import dto.ReviewDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import rest.api.ReviewEndpoint;
import service.api.ReviewService;

import java.util.List;

@RestController
public class ReviewController extends BaseController implements ReviewEndpoint {
    @Autowired
    private ReviewService reviewService;

    @Override
    public ResponseEntity<ReviewDto> addReview(@RequestBody ReviewDto dto) {
        try {
            ReviewDto reviewDto = reviewService.addReview(dto);
            return ResponseEntity.ok(reviewDto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<ReviewDto>> getSprintReviewsByCategory(@PathVariable("sprintKey") String sprintKey,@PathVariable("category") String category) {
        try {
            List<ReviewDto> dtos = reviewService.getSprintReviewsByCategory(sprintKey, category);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

}
