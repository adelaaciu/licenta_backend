package rest.impl;

import dto.BoardUserDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.api.BoardUserEndpoint;
import service.api.BoardUserService;

import java.util.List;

@RestController
public class BoardUserController extends BaseController implements BoardUserEndpoint {
    @Autowired
    private BoardUserService boardUserService;

    @Override
    public ResponseEntity<List<BoardUserDto>> updateBoardUsers(@PathVariable("boardKey") String boardKey, @RequestBody List<String> usersMail) {
        try {
            List<BoardUserDto> dtos = boardUserService.updateBoardUsers(boardKey, usersMail);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<BoardUserDto>> getAllBoardUsers(@PathVariable("boardKey")String boardKey) {
        try {
            List<BoardUserDto> dtos = boardUserService.getAllBoardUsers(boardKey);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<BoardUserDto> getCurrentBoardUser(@PathVariable("boardKey") String boardKey) {
        try {
            BoardUserDto dto = boardUserService.getCurrentBoardUser(boardKey);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<BoardUserDto> addWatcherToTask(@PathVariable("taskKey") String taskKey, @RequestBody  BoardUserDto boardUserDto) {
        try {
            BoardUserDto dto = boardUserService.addWatcherToTask(taskKey, boardUserDto);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<BoardUserDto> addAssigneeToTask(@PathVariable("taskKey") String taskKey, @RequestBody BoardUserDto boardUserDto) {
        try {
            BoardUserDto dto = boardUserService.addAssigneeToTask(taskKey, boardUserDto);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }
}
