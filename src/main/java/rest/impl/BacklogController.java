package rest.impl;

import dto.BacklogDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.api.BacklogEndpoint;
import service.api.BacklogService;

@RestController
public class BacklogController extends BaseController implements BacklogEndpoint {
    @Autowired
    private BacklogService backlogService;
    @Override
    public ResponseEntity<BacklogDto> createBacklog(@PathVariable("boardKey") String boardKey, @RequestBody BacklogDto backlog) {
        try {
            BacklogDto backlogDto = backlogService.createBacklog(boardKey, backlog);
            return ResponseEntity.ok(backlogDto);
        } catch (AbstractException e) {
            return createResponse(e);
        }

    }

    @Override
    public ResponseEntity<BacklogDto> getBacklog(@PathVariable("backlogKey") String backlogKey) {
        try {
            BacklogDto backlogDto = backlogService.getBacklog(backlogKey);
            return ResponseEntity.ok(backlogDto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }
}
