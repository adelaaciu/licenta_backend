package rest.impl;

import dto.ProjectDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.api.ProjectEndpoint;
import service.api.ProjectService;

import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
public class ProjectController extends BaseController implements ProjectEndpoint {
    @Autowired
    private ProjectService projectService;

    @Override
    public ResponseEntity<List<ProjectDto>> getAllProjects() {
        try {
            List<ProjectDto> dtos = projectService.getAllProjects();
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<ProjectDto>> getProjectsByNameContaining(@PathVariable("name") String name) {
        try {
            List<ProjectDto> dtos = projectService.getProjectsByNameContaining(name);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<ProjectDto> getProjectsByName(@PathVariable("name") String name) {
        try {
            ProjectDto dto = projectService.getProjectsByName(name);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity createProject(@RequestBody ProjectDto projectDTO) {
        try {
            final long start = System.currentTimeMillis();
            final ProjectDto dto = projectService.createProject(projectDTO);
            final long end = System.currentTimeMillis();

//            System.out.println("Took: " + (end - start) );

            return  ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity deleteProject(@RequestBody String name) {
        try {
            projectService.deleteProject(name);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<ProjectDto>> getProjectsByUser() {
        try {
            List<ProjectDto> projectUsers = projectService.getProjectsByUser();
            return ResponseEntity.ok(projectUsers);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }
}
