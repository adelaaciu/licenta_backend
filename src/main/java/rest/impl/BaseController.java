package rest.impl;

import exception.AbstractException;
import exception.ExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

public class BaseController {
    public ResponseEntity createResponse(AbstractException e) {
        switch (e.getType()) {
            case FAILURE:
                //TODO: Check if we should return 500s
                return ResponseEntity.badRequest().body(new ExceptionResponse().status(HttpStatus.BAD_REQUEST).message(e.getMessage()));

            case INVALID:
                return ResponseEntity.badRequest().body(new ExceptionResponse().status(HttpStatus.BAD_REQUEST).message(e.getMessage()));

            case FORBIDDEN:
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ExceptionResponse().status(HttpStatus.FORBIDDEN).message(e.getMessage()));

            case MISSING:
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ExceptionResponse().status(HttpStatus.NOT_FOUND).message(e.getMessage()));

            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ExceptionResponse().status(HttpStatus.UNAUTHORIZED).message(e.getMessage()));

            default:
                return ResponseEntity.badRequest().body(new ExceptionResponse().status(HttpStatus.BAD_REQUEST).message(e.getMessage()));
        }
    }

    public ResponseEntity createResponse(IOException e) {
        return ResponseEntity.badRequest().body(new ExceptionResponse().status(HttpStatus.BAD_REQUEST).message(e.getMessage()));
    }
}
