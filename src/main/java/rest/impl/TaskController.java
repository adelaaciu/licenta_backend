package rest.impl;

import dto.DashboardDto;
import dto.TaskDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rest.api.TaskEndpoint;
import service.api.TaskService;

import java.util.List;

@RestController
public class TaskController extends BaseController implements TaskEndpoint {
    @Autowired
    private TaskService taskService;

    @Override
    public ResponseEntity<TaskDto> changePriority(@PathVariable("taskKey") String taskKey, @PathVariable("priority") String priority) {
        try {
            TaskDto sprintTaskDto = taskService.changePriority(taskKey, priority);
            return ResponseEntity.ok(sprintTaskDto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<TaskDto> updateTaskStatus(@PathVariable("taskKey") String taskKey, @PathVariable("status") String status) {
        try {
            TaskDto sprintTaskDto = taskService.updateTaskStatus(taskKey, status);
            return ResponseEntity.ok(sprintTaskDto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<TaskDto> addStoryPointsToTask(@PathVariable("taskKey") String taskKey, @PathVariable("storyPoints") int storyPoints) {
        try {
            TaskDto dto = taskService.addStoryPointsToTask(taskKey, storyPoints);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<DashboardDto>> getLatestUpdates() {
        try {
            List<DashboardDto> dtos = taskService.getLatestUpdates();
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }
}
