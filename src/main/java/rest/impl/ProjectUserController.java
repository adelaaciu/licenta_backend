package rest.impl;

import dto.ProjectUserDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.api.ProjectUserEndpoint;
import service.api.ProjectUserService;

import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
public class ProjectUserController extends BaseController implements ProjectUserEndpoint {
    @Autowired
    private ProjectUserService projectUserService;

    @Override
    public ResponseEntity<List<ProjectUserDto>> getProjectUsersByUserMailContaining(@RequestParam("userMail") String userMail) {
        try {
            List<ProjectUserDto> projectUsers = projectUserService.getAllByUserMail(userMail);
            return ResponseEntity.ok(projectUsers);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<ProjectUserDto> getProjectUserByUserMailAndProject( @PathVariable("projectName") String projectName) {
        try {
            ProjectUserDto projectUser = projectUserService.getProjectUserByUserMailAndProjectName(projectName);
            return ResponseEntity.ok(projectUser);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<ProjectUserDto>> getProjectUsers(@PathVariable("projectName") String projectName) {
        try {
            List<ProjectUserDto> projectUsers = projectUserService.getProjectUsers(projectName);
            return ResponseEntity.ok(projectUsers);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<ProjectUserDto>> updateProjectUsers(@PathVariable("projectName") String projectName, @RequestBody List<String> usersMail) {
        try {
            List<ProjectUserDto> projectUsers = projectUserService.updateProjectUsers(projectName, usersMail);
            return ResponseEntity.ok(projectUsers);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<ProjectUserDto> addProjectOwner(@PathVariable("projectName") String projectName, @RequestBody String pmMail) {
        try {
            final long start = System.currentTimeMillis();
            ProjectUserDto projectUser = projectUserService.addProjectOwner(projectName, pmMail);
            final long end = System.currentTimeMillis();

            System.out.println("Took: " + (end - start) );

            return ResponseEntity.ok(projectUser);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity deleteProjectUser(@PathVariable("projectName") String projectName, @RequestParam("userMail") String userMail) {
        try {
            projectUserService.deleteProjectUserByParams(projectName, userMail);
            return ResponseEntity.ok().build();
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }


}
