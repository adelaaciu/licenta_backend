package rest.impl;

import dto.SprintDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.api.SprintEndpoint;
import service.api.SprintService;

import java.util.List;

@RestController
public class SprintController extends BaseController implements SprintEndpoint {
    @Autowired
    private SprintService sprintService;

    @Override
    public ResponseEntity<SprintDto> endSprint(@PathVariable("sprintKey") String sprintKey) {
        try {
            SprintDto dto = sprintService.endSprint(sprintKey);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<SprintDto> startSprint(@PathVariable("sprintKey") String sprintKey, @RequestBody int sprintDays) {
        try {
            SprintDto dto = sprintService.startSprint(sprintKey, sprintDays);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<SprintDto> createSprint(@RequestBody SprintDto dto) {
        try {
            SprintDto sprintDto = sprintService.createSprint(dto);
            return ResponseEntity.ok(sprintDto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<SprintDto>> getBoardSprints(@PathVariable("boardKey") String boardKey) {
        try {
            List<SprintDto> dtos = sprintService.getBoardSprints(boardKey);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<SprintDto> getSprint(@PathVariable("sprintKey") String sprintKey) {
        try {
            SprintDto dto = sprintService.getSprint(sprintKey);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<SprintDto> getActiveSprint(@PathVariable("boardKey") String boardKey) {
        try {
            SprintDto dto = sprintService.getActiveSprint(boardKey);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity deleteSprint(@PathVariable("sprintKey") String sprintKey) {
        try {
            sprintService.deleteSprint(sprintKey);
            return ResponseEntity.ok().build();
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }
}
