package rest.impl;

import dto.BacklogTaskDto;
import dto.SprintTaskDto;
import dto.TaskDto;
import exception.AbstractException;
import model.BacklogTaskEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.api.SprintTaskEndpoint;
import service.api.BacklogTaskService;
import service.api.SprintTaskService;
import service.api.TaskService;

import java.util.List;

@RestController
public class SprintTaskController extends BaseController implements SprintTaskEndpoint {
    @Autowired
    private SprintTaskService sprintTaskService;

    @Override
    public ResponseEntity<SprintTaskDto> addTaskToSprint(@PathVariable("sprintKey") String sprintKey, @PathVariable("taskKey") String taskKey) {
        try {
            SprintTaskDto sprintTaskDto = sprintTaskService.addTaskToSprint(sprintKey, taskKey);
            return ResponseEntity.ok(sprintTaskDto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }


    @Override
    public ResponseEntity<List<SprintTaskDto>> getAllSprintTasks(@PathVariable("sprintKey") String sprintKey) {
        try {
            List<SprintTaskDto> dtos = sprintTaskService.getAllSprintTasks(sprintKey);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<SprintTaskDto> updateTask(@RequestBody SprintTaskDto task, @PathVariable("boardKey") String boardKey) {
        try {
            SprintTaskDto dto = sprintTaskService.updateTask(task, boardKey);
            return ResponseEntity.ok(dto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<SprintTaskDto>> getByTitleContaining(@PathVariable("title") String title) {
        try {
            List<SprintTaskDto> dtos = sprintTaskService.getByTitleContaining(title);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<SprintTaskDto>> getUserTasks() {
        try {
            List<SprintTaskDto> dtos = sprintTaskService.getUserTasks();
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<SprintTaskDto>> getDoneTasks(@PathVariable("sprintKey") String sprintKey) {
        try {
            List<SprintTaskDto> dtos = sprintTaskService.getDoneTasks(sprintKey);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }


}
