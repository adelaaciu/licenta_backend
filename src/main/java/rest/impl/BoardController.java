package rest.impl;

import dto.BoardDto;
import exception.AbstractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.api.BoardEndpoint;
import service.api.BoardService;

import java.util.List;

@RestController
public class BoardController extends BaseController implements BoardEndpoint {
    @Autowired
    private BoardService boardService;

    @Override
    public ResponseEntity<BoardDto> getBoard(@PathVariable("key") String key) {
        try {
            BoardDto boardDto = boardService.getBoard(key);
            return ResponseEntity.ok(boardDto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<List<BoardDto>> getProjectBoardList(@PathVariable("projectKey") String projectKey) {
        try {
            List<BoardDto> dtos = boardService.getProjectBoards(projectKey);
            return ResponseEntity.ok(dtos);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity<BoardDto> createBoard(@RequestBody BoardDto dto) {
        try {
            BoardDto boardDto = boardService.createBoard(dto);
            return ResponseEntity.ok(boardDto);
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }

    @Override
    public ResponseEntity deleteBoard(@PathVariable("boardKey") String boardKey) {
        try {
            boardService.deleteBoard(boardKey);
            return ResponseEntity.ok().build();
        } catch (AbstractException e) {
            return createResponse(e);
        }
    }
}
