package rest.api;

import dto.BacklogTaskDto;
import dto.TaskDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/backlogTask")
public interface BacklogTaskEndpoint {
    @PostMapping
    ResponseEntity<BacklogTaskDto> createTask(BacklogTaskDto task);

    @PutMapping("/list/board/{boardKey}")
    ResponseEntity<BacklogTaskDto> updateTask(BacklogTaskDto task, String boardKey);

    @DeleteMapping("/list/{taskKey}")
    ResponseEntity removeTaskFromBacklog(String taskKey);

    @GetMapping("/list/{backlogKey}")
    ResponseEntity<List<BacklogTaskDto>> getAllBacklogTasks(String backlogKey);

    @DeleteMapping("/list/{sprintKey}/tasks/{taskKey}")
    ResponseEntity addTaskFromSprintToBacklog(String sprintKey, String taskKey);

    @GetMapping("/list/containing/{title}")
    ResponseEntity<List<BacklogTaskDto>> getBacklogByTitleContaining(String title);
}
