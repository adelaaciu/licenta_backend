package rest.api;

import dto.SprintDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/sprint")
public interface SprintEndpoint {
    @PutMapping("list/{sprintKey}/end")
    ResponseEntity<SprintDto> endSprint(String sprintKey);

    @PutMapping("list/{sprintKey}/start")
    ResponseEntity<SprintDto> startSprint(String sprintKey, int sprintDays);

    @PostMapping()
    ResponseEntity<SprintDto> createSprint(SprintDto dto);

    @GetMapping("/list/{boardKey}")
    ResponseEntity<List<SprintDto>> getBoardSprints(String boardKey);

    @GetMapping("/{sprintKey}")
    ResponseEntity<SprintDto> getSprint(String sprintKey);

    @GetMapping("/list/active/{boardKey}")
    ResponseEntity<SprintDto> getActiveSprint(String boardKey);

    @DeleteMapping("/list/{sprintKey}")
    ResponseEntity deleteSprint(String sprintKey);
}
