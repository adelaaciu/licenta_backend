package rest.api;

import dto.BoardDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/board")
public interface BoardEndpoint {
    @GetMapping("/{key}")
    ResponseEntity<BoardDto> getBoard(String key);

    @GetMapping("list/project/{projectKey}")
    ResponseEntity<List<BoardDto>> getProjectBoardList(String projectKey);

    @PostMapping
    ResponseEntity<BoardDto> createBoard(BoardDto dto);

    @DeleteMapping("/list/{boardKey}")
    ResponseEntity deleteBoard(String boardKey);
}
