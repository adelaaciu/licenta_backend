package rest.api;

import dto.ProjectUserDto;
import model.ProjectEntity;
import model.ProjectUserEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/projectUser")
@CrossOrigin
public interface ProjectUserEndpoint {

    @GetMapping("/list/")
    ResponseEntity<List<ProjectUserDto>> getProjectUsersByUserMailContaining(String userMail);


    @GetMapping("/list/user/project/{projectName}")
    ResponseEntity<ProjectUserDto> getProjectUserByUserMailAndProject(String projectName);

    @GetMapping("/list/project/{projectName}")
    ResponseEntity<List<ProjectUserDto>> getProjectUsers(String projectName);

    @PutMapping("/list/project/{projectName}/users")
    ResponseEntity<List<ProjectUserDto>> updateProjectUsers(String projectName, List<String> usersMail);


    @PutMapping("/list/project/{projectName}/users/owner")
    ResponseEntity<ProjectUserDto> addProjectOwner(String projectName, String pmMail);

    @DeleteMapping("/list/project/{projectName}/users/delete")
    ResponseEntity deleteProjectUser(String projectName, String userMail);
}
