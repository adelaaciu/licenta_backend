package rest.api;

import dto.SprintTaskDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/sprintTask")
public interface SprintTaskEndpoint {
    @PutMapping("/list/{taskKey}/sprint/{sprintKey}")
    ResponseEntity<SprintTaskDto> addTaskToSprint(String sprintKey, String taskKey);

    @GetMapping("/list/{sprintKey}")
    ResponseEntity<List<SprintTaskDto>> getAllSprintTasks(String sprintKey);

    @PutMapping("/list/board/{boardKey}")
    ResponseEntity<SprintTaskDto> updateTask(SprintTaskDto task, String boardKey);

    @GetMapping("/list/containing/{title}")
    ResponseEntity<List<SprintTaskDto>> getByTitleContaining(String title);

    @GetMapping("/list/users")
    ResponseEntity<List<SprintTaskDto>> getUserTasks();

    @GetMapping("/list/doneTasks/{sprintKey}")
    ResponseEntity<List<SprintTaskDto>> getDoneTasks(String sprintKey);
}
