package rest.api;

import dto.ProjectDto;
import dto.ProjectUserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/project")
@CrossOrigin
public interface ProjectEndpoint {
    @GetMapping("/list")
    ResponseEntity<List<ProjectDto>> getAllProjects();

    @GetMapping("/list/{name}")
    ResponseEntity<List<ProjectDto>> getProjectsByNameContaining(String name);

    @GetMapping("/{name}")
    ResponseEntity<ProjectDto> getProjectsByName(String name);

    @PostMapping
    ResponseEntity createProject(ProjectDto ProjectDto);

    @PutMapping("/list/delete")
    ResponseEntity deleteProject(String name);

    @GetMapping("/list/users")
    ResponseEntity<List<ProjectDto>> getProjectsByUser();
}
