package rest.api;

import dto.CommentDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/comment")
public interface CommentEndpoint {
    @PutMapping()
    ResponseEntity<CommentDto> addComment(CommentDto comment);

    @GetMapping("/list/{taskKey}")
    ResponseEntity<List<CommentDto>> getComments(String taskKey);
}
