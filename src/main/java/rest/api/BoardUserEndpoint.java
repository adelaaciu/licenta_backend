package rest.api;

import dto.BoardUserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/boardUser")
@CrossOrigin
public interface BoardUserEndpoint {
    @PutMapping("/list/board/{boardKey}")
    ResponseEntity<List<BoardUserDto>> updateBoardUsers(String boardKey, List<String> usersMail);

    @GetMapping("/list/{boardKey}")
    ResponseEntity<List<BoardUserDto>> getAllBoardUsers(String boardKey);

    @GetMapping("/list/board/{boardKey}")
    ResponseEntity<BoardUserDto> getCurrentBoardUser(String boardKey);

    @PutMapping("/list/{taskKey}/watcher")
    ResponseEntity<BoardUserDto> addWatcherToTask(String taskKey, BoardUserDto dto);

    @PutMapping("/list/{taskKey}/assignee")
    ResponseEntity<BoardUserDto> addAssigneeToTask(String taskKey, BoardUserDto dto);
}
