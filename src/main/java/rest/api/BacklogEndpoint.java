package rest.api;

import dto.BacklogDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/backlog")
public interface
BacklogEndpoint {
    @PostMapping("/board/{boardKey}")
    ResponseEntity<BacklogDto> createBacklog( String boardKey, BacklogDto backlog);

    @GetMapping("/list/{backlogKey}")
    ResponseEntity<BacklogDto> getBacklog(String backlogKey);
}
