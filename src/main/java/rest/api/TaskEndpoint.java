package rest.api;

import dto.BacklogTaskDto;
import dto.DashboardDto;
import dto.TaskDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/task")
public interface TaskEndpoint {

    @PutMapping("/list/{taskKey}/priority/{priority}")
    ResponseEntity<TaskDto> changePriority(String taskKey, String priority);

    @PutMapping("/{taskKey}/status/{status}")
    ResponseEntity<TaskDto> updateTaskStatus(String taskKey, String status);

    @PutMapping("/list/{taskKey}/storyPoints")
    ResponseEntity<TaskDto> addStoryPointsToTask(String taskKey, int storyPoints);

    @GetMapping("/list/topTen/")
    ResponseEntity<List<DashboardDto>> getLatestUpdates();
}
