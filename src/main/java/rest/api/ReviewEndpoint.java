package rest.api;

import dto.ReviewDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/review")
@CrossOrigin
public interface ReviewEndpoint {
    @PostMapping
    ResponseEntity<ReviewDto> addReview(ReviewDto dto);

    @GetMapping("/sprint/{sprintKey}/category/{category}")
    ResponseEntity<List<ReviewDto>> getSprintReviewsByCategory(String sprintKey, String category);
}
