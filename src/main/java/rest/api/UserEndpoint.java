package rest.api;

import dto.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Interface used to make request for users.
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public interface UserEndpoint {
    @GetMapping("/list")
    ResponseEntity<List<UserDto>> getAllUsers();

    @GetMapping("/list/{name}")
    ResponseEntity<List<UserDto>> getUsersByName(String name);

    @PostMapping
    ResponseEntity createUser(UserDto UserDto);

    @PutMapping("/update")
    ResponseEntity<UserDto> updateUser(UserDto UserDto);

    @DeleteMapping("/list/delete")
    ResponseEntity deleteUser(String email);

    @GetMapping("/loggedUser")
    ResponseEntity<UserDto> getCurrentUser();

    @GetMapping("/userRole/{role}")
    ResponseEntity<Boolean> isCurrentUserInRole(String role);
}

