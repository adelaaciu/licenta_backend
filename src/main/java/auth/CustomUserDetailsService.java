package auth;

import exception.user.UserNotFoundException;
import model.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repository.UserRepository;

import java.util.Collections;

@Component
public class CustomUserDetailsService implements UserDetailsService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public UserDetails loadUserByUsername(final String username) {
        log.debug("Authenticating: {}.", username);
//        System.out.println("************************** loadUserByUsername" + this.getClass());
        UserEntity user = userRepository.findByMail(username);

        if (user == null)
            throw new UserNotFoundException();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(user.getRole());

        return new org.springframework.security.core.userdetails.User(username.toLowerCase(),
                user.getPassword(),
                Collections.singletonList(grantedAuthority));
    }
}
