package exception.mail;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class MailContentNotFoundException extends AbstractException {
    @Override
    public String getMessage() {
        return "Empty content";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.MISSING;
    }
}
