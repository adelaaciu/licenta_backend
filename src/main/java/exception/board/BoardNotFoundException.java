package exception.board;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class BoardNotFoundException extends AbstractException {
    @Override
    public String getMessage() {
        return "Board does not exist";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.MISSING;
    }
}
