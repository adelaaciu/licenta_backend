package exception.board;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class BoardAlreadyExistException extends AbstractException {
    @Override
    public String getMessage() {
        return "This board already exist";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.INVALID;
    }
}
