package exception.task;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class BacklogTaskNotFoundException extends AbstractException {
    @Override
    public String getMessage() {
        return "Backlog does not have tasks";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.MISSING;
    }
}
