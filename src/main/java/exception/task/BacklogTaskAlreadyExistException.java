package exception.task;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class BacklogTaskAlreadyExistException extends AbstractException {
    @Override
    public String getMessage() {
        return "Task already exist in backlog";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.FAILURE;
    }
}
