package exception.task;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class TaskAlreadyExistException extends AbstractException {
    @Override
    public String getMessage() {
        return "Project already exist in database!";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.INVALID;
    }
}
