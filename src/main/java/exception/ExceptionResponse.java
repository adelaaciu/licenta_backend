package exception;

import org.springframework.http.HttpStatus;

public class ExceptionResponse {
    private HttpStatus status;
    private String message;

    public HttpStatus getStatus() {
        return status;
    }

    public ExceptionResponse status(HttpStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public ExceptionResponse message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
