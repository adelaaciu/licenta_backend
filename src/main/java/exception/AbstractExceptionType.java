package exception;

public enum AbstractExceptionType {// Sperical Rights required
    UNAUTHORIZED,
    // User is not allowed to perform this operation
    FORBIDDEN,
    /**
     * Invalid data was provided - generate a 400
     */
    INVALID,
    /**
     * Missing data - return a 404
     */
    MISSING,
    // Something went really bad
    FAILURE
}
