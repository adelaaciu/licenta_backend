package exception;

public abstract class AbstractException extends RuntimeException {
    protected String message;

    public AbstractException() {
    }

    public AbstractException(String message) {
        super(message);
    }

    public abstract String getMessage();

    public abstract AbstractExceptionType getType();
}
