package exception.project;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class ProjectNotFoundException extends AbstractException {
    @Override
    public String getMessage() {
        return "The project does not exist!";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.MISSING;
    }
}
