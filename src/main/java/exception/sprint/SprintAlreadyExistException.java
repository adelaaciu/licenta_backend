package exception.sprint;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class SprintAlreadyExistException extends AbstractException {
    @Override
    public String getMessage() {
        return "The sprint already exist in database!";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.INVALID;
    }
}
