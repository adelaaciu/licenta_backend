package exception.sprint;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class CanNotCreateSprint extends AbstractException {
    @Override
    public String getMessage() {
        return "Last sprint is not over yet";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.FORBIDDEN;
    }
}
