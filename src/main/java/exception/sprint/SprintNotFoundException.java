package exception.sprint;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class SprintNotFoundException extends AbstractException {
    @Override
    public String getMessage() {
        return "The sprint does not exist!";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.MISSING;
    }
}
