package exception.backlog;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class BacklogAlreadyExistException extends AbstractException {
    @Override
    public String getMessage() {
        return "You already have a backlog on the project";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.FORBIDDEN;
    }
}
