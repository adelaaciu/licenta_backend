package exception.backlog;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class BacklogNotFoundException extends AbstractException {
    @Override
    public String getMessage() {
        return "You don'y have a backlog";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.MISSING;
    }
}
