package exception.user;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class BoardUsersNotFoundException extends AbstractException {
    @Override
    public String getMessage() {
        return "Users are not assigned to this board";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.MISSING;
    }
}
