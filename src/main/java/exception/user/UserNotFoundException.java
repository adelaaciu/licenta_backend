package exception.user;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class UserNotFoundException extends AbstractException {
    @Override
    public String getMessage() {
        return "User does not exist!";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.FAILURE;
    }
}
