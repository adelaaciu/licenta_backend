package exception.user;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class ProjectUsersNotFoundException extends AbstractException {
    @Override
    public String getMessage() {
        return "Project does not have assigned user";
    }

    @Override
    public AbstractExceptionType getType() {
        return null;
    }
}
