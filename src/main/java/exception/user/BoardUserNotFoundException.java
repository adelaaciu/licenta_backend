package exception.user;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class BoardUserNotFoundException extends AbstractException {
    @Override
    public String getMessage() {
        return "User not assigned to this board";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.MISSING;
    }
}
