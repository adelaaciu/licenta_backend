package exception.user;

import exception.AbstractException;
import exception.AbstractExceptionType;

public class UserAlreadyExistException extends AbstractException {
    @Override
    public String getMessage() {
        return "User already exist in database!";
    }

    @Override
    public AbstractExceptionType getType() {
        return AbstractExceptionType.INVALID;
    }
}
