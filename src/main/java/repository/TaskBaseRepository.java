package repository;

import model.TaskEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskBaseRepository<T extends TaskEntity> extends PagingAndSortingRepository<T, Long> {
    T findByKey(String key);
    List<T> findByAssigneeUserUserMail(String userMail);
    List<T> findByWatcherUserUserMail(String userMail);
    List<TaskEntity> findTop10ByAssigneeUserUserMailOrWatcherUserUserMailOrderByActualizationDate(String assigneeMail, String watcherMail);
}
