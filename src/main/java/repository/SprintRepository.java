package repository;

import model.SprintEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SprintRepository extends PagingAndSortingRepository<SprintEntity, Long> {
    SprintEntity findByKey(String key);
    List<SprintEntity> findAllByBoardKey(String boardKey);
}
