package repository;

import model.BacklogEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BacklogRepository extends PagingAndSortingRepository<BacklogEntity, Long> {
    BacklogEntity findByKey(String key);
}
