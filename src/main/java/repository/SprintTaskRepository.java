package repository;

import model.SprintTaskEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

public interface SprintTaskRepository extends TaskBaseRepository<SprintTaskEntity> {
    List<SprintTaskEntity> findAllBySprintKey(String sprintKey);
    List<SprintTaskEntity> findAllBySprintKeyOrderByEndDate(String sprintKey);
    List<SprintTaskEntity> findAllByTitleContainingIgnoringCase(String title);
    List<SprintTaskEntity> findAllByAssigneeUserUserMail(String mail);
}
