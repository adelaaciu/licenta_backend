package repository;

import model.ReviewEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends PagingAndSortingRepository<ReviewEntity, Long> {
    List<ReviewEntity> findAllBySprintKeyAndCategory(String sprintKey, String category);
    List<ReviewEntity> findAllBySprintKey(String sprintKey);
    List<ReviewEntity> findAllByUserUserUserMail(String userMail);
}
