package repository;

import model.TaskEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

@Transactional
public interface TaskRepository extends TaskBaseRepository<TaskEntity> {
}
