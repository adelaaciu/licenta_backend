package repository;

import model.BoardEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoardRepository extends PagingAndSortingRepository<BoardEntity, Long> {
    BoardEntity findByKey(String key);
    BoardEntity findByBacklogKey(String backlogKey);
    List<BoardEntity> findAllByProjectName(String name);
}
