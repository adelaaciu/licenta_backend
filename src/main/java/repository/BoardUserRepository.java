package repository;

import model.BoardUserEntity;
import model.ProjectEntity;
import model.ProjectUserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoardUserRepository extends PagingAndSortingRepository<BoardUserEntity, Long> {
    List<BoardUserEntity> findAllByBoardKey(String boardKey);
    BoardUserEntity findByUserUserMailAndBoardKey(String mail, String boardKey);
    List<BoardUserEntity> findAllByUserProjectName(String projectName);
    BoardUserEntity findByUserUserMail(String mail);
    BoardUserEntity findByUserUserMailAndBoardProjectName(String mail, String projectName);
//    BoardUserEntity findByUserMailUserAndBoardKey(String mail, String boardKey);

    void deleteAllByBoardKey(String boardKey);

}
