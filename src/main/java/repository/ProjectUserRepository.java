package repository;

import model.ProjectUserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProjectUserRepository extends PagingAndSortingRepository<ProjectUserEntity, Long> {
    List<ProjectUserEntity> findAllByProjectName(String name);
    List<ProjectUserEntity> findByUserMailContaining(String mail);
    ProjectUserEntity findByUserMail(String mail);
    ProjectUserEntity findByUserMailAndProjectName(String mail, String projectName);
}
