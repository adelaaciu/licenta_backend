package repository;

import model.BacklogTaskEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
public interface BacklogTaskRepository extends TaskBaseRepository<BacklogTaskEntity> {
    List<BacklogTaskEntity> findAllByBacklogKey(String backlogKey);
    List<BacklogTaskEntity> findAllByTitleContainingIgnoringCase(String title);
    BacklogTaskEntity findByTitle(String title);
}
