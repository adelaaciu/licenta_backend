package repository;

import model.BoardUserEntity;
import model.CommentEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends PagingAndSortingRepository<CommentEntity, Long> {
    CommentEntity findByKey(String key);
    List<CommentEntity> findAllByTaskKey(String key);
    List<CommentEntity> findAllByUser(BoardUserEntity user);
    List<CommentEntity> findAllByUserUserUserMail(String userMail);


}
