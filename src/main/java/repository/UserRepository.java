package repository;

import model.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Used to access users from DB
 */
@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {
    List<UserEntity> findAll();
    UserEntity findByMail(String mail);
    List<UserEntity> findAllByFirstNameContainingOrLastNameContainingIgnoringCase(String firstName, String lastName);
}

