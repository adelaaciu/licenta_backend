package repository;

import model.ProjectEntity;
import model.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends PagingAndSortingRepository<ProjectEntity, Long> {
    List<ProjectEntity> findAll();

    List<ProjectEntity> findAllByNameContainingIgnoringCase(String name);

    ProjectEntity findByName(String name);
}
