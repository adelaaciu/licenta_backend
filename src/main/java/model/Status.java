package model;

public enum Status {
    TODO("To-Do"),
    DEVIMPL("Dev-implementation"),
    DEVTEST("Dev-testing"),
    REVIEW("Review"),
    QA("QA"),
    DONE("Done");

    private String message;

    Status(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
