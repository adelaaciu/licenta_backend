package model;

import dto.BoardDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Table(name = "Board")
@Entity
@Getter
@Setter
public class BoardEntity extends BaseEntity<BoardDto> {
    @NotNull
    @Column(unique = true)
    private String key;
    private String name;
    @OneToOne
    private BacklogEntity backlog;
    @OneToOne
    private ProjectEntity project;
    @OneToMany(cascade = CascadeType.ALL)
    private List<SprintEntity> sprints;
    @OneToMany(cascade = CascadeType.ALL)
    private List<BoardUserEntity> boardUsers;

    @Override
    public BoardDto toDto() {
        BoardDto dto = new BoardDto();
        dto.setKey(key);
        dto.setName(name);
        if (backlog != null)
            dto.setBacklog(backlog.toDto());
        if (project != null)
            dto.setProject(project.toDto());
        return dto;
    }

    @Override
    public BoardEntity fromDto(BoardDto dto) {
        key = dto.getKey();
        name = dto.getName();
        if (dto.getBacklog() != null)
            backlog = new BacklogEntity().fromDto(dto.getBacklog());
        if (dto.getProject() != null)
            project = new ProjectEntity().fromDto(dto.getProject());
        return this;
    }
}
