package model;

import dto.DashboardDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Table(name = "Dashboard")
@Entity
public class DashboardEntity extends BaseEntity<DashboardDto >{
    @Override
    public DashboardDto toDto() {
        return null;
    }

    @Override
    public DashboardEntity fromDto(DashboardDto dto) {
        return null;
    }
}
