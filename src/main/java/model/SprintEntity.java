package model;

import dto.SprintDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Table(name = "Sprint")
@Entity
public class SprintEntity extends BaseEntity<SprintDto> {
    @NotNull
    @Column(unique = true)
    private String key;
    private int sprintNo;
    @OneToOne
    private BoardEntity board;
    @OneToMany
    private List<SprintTaskEntity> tasks;
    private Date startDate;
    private Date endDate;
    private boolean closed;
    private int days;

    @Override
    public SprintDto toDto() {
        SprintDto dto = new SprintDto();
        dto.setKey(key);
        dto.setSprintNo(sprintNo);
        dto.setBoard(board.toDto());
        dto.setStartDate(startDate);
        dto.setEndDate(endDate);
        dto.setClosed(closed);
        dto.setDays(days);
        return dto;
    }

    @Override
    public SprintEntity fromDto(SprintDto dto) {
        key = dto.getKey();
        sprintNo = dto.getSprintNo();
        board = new BoardEntity().fromDto(dto.getBoard());
        startDate = dto.getStartDate();
        endDate = dto.getEndDate();
        closed = dto.isClosed();
        days = dto.getDays();

        return this;
    }
}
