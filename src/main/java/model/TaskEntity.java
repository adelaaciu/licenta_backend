package model;

import dto.TaskDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Table(name = "Task")
@Entity
public class TaskEntity extends BaseEntity<TaskDto> {
    @NotNull
    @Column(unique = true)
    protected String key;
    protected String title;
    protected String description;
    protected String status;
    @OneToOne
    protected BoardUserEntity watcher;
    @OneToOne
    protected BoardUserEntity assignee;
    protected Date startDate;
    protected Date endDate;
    protected Date actualizationDate;
    @OneToMany
    protected List<CommentEntity> comments;
    protected int storyPoints;
    protected String priority;

    @Override
    public TaskDto toDto() {
        TaskDto dto = new TaskDto();
        dto.setKey(this.key);
        dto.setTitle(this.title);
        dto.setDescription(this.description);
        dto.setStatus(this.status);
        dto.setStartDate(this.startDate);
        dto.setEndDate(this.endDate);
        dto.setActualizationDate(this.actualizationDate);
        dto.setStoryPoints(this.storyPoints);
        dto.setPriority(this.priority);
        if (getWatcher() != null) {
            dto.setWatcher(this.watcher.toDto());
        }
        if (getAssignee() != null) {
            dto.setAssignee(this.assignee.toDto());
        }
        return dto;
    }

    @Override
    public TaskEntity fromDto(TaskDto dto) {
        this.setKey(dto.getKey());
        this.setTitle(dto.getTitle());
        this.setDescription(dto.getDescription());
        this.setStatus(dto.getStatus());
        this.setStartDate(dto.getStartDate());
        this.setEndDate(dto.getEndDate());
        this.setActualizationDate(dto.getActualizationDate());
        this.setStoryPoints(dto.getStoryPoints());
        this.setPriority(dto.getPriority());
        return this;
    }
}
