package model;

public enum Priority {
    LOW("Low"),
    MEDIUM("Medium"),
    HIGH("High");

    private String message;

    Priority(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
