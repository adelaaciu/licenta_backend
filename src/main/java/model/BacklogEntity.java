package model;

import dto.BacklogDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Table(name = "Backlog")
@Entity
@Getter
@Setter
public class BacklogEntity  extends BaseEntity<BacklogDto> {
    @NotNull
    @Column(unique = true)
    private String key;
    @OneToMany
    private List<BacklogTaskEntity> tasks;

    @Override
    public BacklogDto toDto() {
        BacklogDto dto = new BacklogDto();
        dto.setKey(this.key);
        return dto;
    }

    @Override
    public BacklogEntity fromDto(BacklogDto dto) {
        this.key = dto.getKey();
//        if (dto.getBoard() != null)
//            this.board = new BoardEntity().fromDto(dto.getBoard());

        return this;
    }
}
