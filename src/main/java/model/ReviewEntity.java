package model;

import dto.ReviewDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "review")
public class ReviewEntity extends BaseEntity<ReviewDto> {
    private String category;
    @OneToOne
    private BoardUserEntity user;
    private String message;
    @OneToOne
    private SprintEntity sprint;

    @Override
    public ReviewDto toDto() {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setCategory(this.category);
        reviewDto.setUser(user.toDto());
        reviewDto.setMessage(this.message);
        reviewDto.setSprint(this.sprint.toDto());
        return reviewDto;
    }

    @Override
    public ReviewEntity fromDto(ReviewDto dto) {
        this.category = dto.getCategory();
        this.message = dto.getMessage();
        return this;
    }
}
