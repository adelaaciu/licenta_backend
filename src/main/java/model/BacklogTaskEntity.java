package model;

import dto.BacklogTaskDto;
import dto.TaskDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Getter
@Setter
@Table(name = "BoardTask")
@Entity
public class BacklogTaskEntity extends TaskEntity {
    @OneToOne
    private BacklogEntity backlog;

    @Override
    public BacklogTaskDto toDto() {
        BacklogTaskDto dto = new BacklogTaskDto();
        dto.setKey(this.key);
        dto.setTitle(this.title);
        dto.setDescription(this.description);
        dto.setStatus(this.status);
        dto.setStartDate(this.startDate);
        dto.setEndDate(this.endDate);
        dto.setActualizationDate(this.actualizationDate);
        dto.setStoryPoints(this.storyPoints);
        dto.setPriority(this.priority);
        if (getWatcher() != null) {
            dto.setWatcher(this.watcher.toDto());
        }
        if (getAssignee() != null) {
            dto.setAssignee(this.assignee.toDto());
        }
        dto.setBacklog(backlog.toDto());
        return dto;
    }

    @Override
    public BacklogTaskEntity fromDto(TaskDto dto) {
        BacklogTaskEntity backlogTaskEntity = (BacklogTaskEntity) super.fromDto(dto);
        backlogTaskEntity.setBacklog(new BacklogEntity().fromDto(((BacklogTaskDto) dto).getBacklog()));

        return backlogTaskEntity;
    }
}
