package model;

import dto.ProjectUserDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Getter
@Setter
@Table(name = "ProjectUser")
@Entity
public class ProjectUserEntity extends BaseEntity<ProjectUserDto> {
    @OneToOne
    private ProjectEntity project;
    @OneToOne
    private UserEntity user;
    private String projectRole;

    @Override
    public ProjectUserDto toDto() {
        ProjectUserDto dto = new ProjectUserDto();
        dto.setProject(project.toDto());
        dto.setUser(user.toDto());
        dto.setProjectRole(projectRole);
        return dto;
    }

    @Override
    public ProjectUserEntity fromDto(ProjectUserDto dto) {
        this.project = new ProjectEntity().fromDto(dto.getProject());
        this.user = new UserEntity().fromDto(dto.getUser());
        this.projectRole = dto.getProjectRole();
        return this;
    }
}
