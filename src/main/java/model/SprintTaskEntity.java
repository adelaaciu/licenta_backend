package model;

import dto.SprintTaskDto;
import dto.TaskDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Getter
@Setter
@Table(name = "SprintTask")
@Entity
public class SprintTaskEntity extends TaskEntity {
    @OneToOne
    private SprintEntity sprint;

    @Override
    public SprintTaskDto toDto() {
        SprintTaskDto dto = new SprintTaskDto();
        dto.setKey(this.key);
        dto.setTitle(this.title);
        dto.setDescription(this.description);
        dto.setStatus(this.status);
        dto.setStartDate(this.startDate);
        dto.setEndDate(this.endDate);
        dto.setActualizationDate(this.actualizationDate);
        dto.setStoryPoints(this.storyPoints);
        dto.setPriority(this.priority);
        if (getWatcher() != null) {
            dto.setWatcher(this.watcher.toDto());
        }
        if (getAssignee() != null) {
            dto.setAssignee(this.assignee.toDto());
        }
        dto.setSprint(sprint.toDto());
        return dto;
    }

    @Override
    public SprintTaskEntity fromDto(TaskDto dto) {
        SprintTaskEntity sprintTaskEntity = (SprintTaskEntity) super.fromDto(dto);
//        sprintTaskEntity.setSprint(new SprintEntity().fromDto(((SprintTaskDto) dto).getSprint()));

        return sprintTaskEntity;
    }

}
