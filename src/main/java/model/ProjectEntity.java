package model;

import dto.ProjectDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@Table(name = "Project")
@Entity
public class ProjectEntity extends BaseEntity<ProjectDto> {
    @NotNull
    @Column(unique = true)
    private String name;
    @NotNull
    private String description;
    @OneToMany(cascade = CascadeType.ALL)
    private List<ProjectUserEntity> users;
    @OneToMany(cascade = CascadeType.ALL)
    private List<BoardEntity> boards;


    @Override
    public ProjectDto toDto() {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setName(this.name);
        projectDto.setDescription(this.description);
        return projectDto;
    }

    @Override
    public ProjectEntity fromDto(ProjectDto dto) {
        name = dto.getName();
        description = dto.getDescription();
        return this;
    }
}
