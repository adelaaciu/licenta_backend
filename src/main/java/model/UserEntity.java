package model;

import dto.UserDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Table(name = "Base_User")
@Entity
public class UserEntity extends BaseEntity<UserDto> {
    @NotNull
    private String role;
    @NotNull
    @Column(unique = true)
    private String mail;
    @NotNull
    private String password;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    private String phoneNumber;
    private String birthday;

    @Override
    public UserDto toDto() {
        UserDto dto = new UserDto();
        dto.setFirstName(this.firstName);
        dto.setLastName(this.lastName);
        dto.setMail(this.mail);
        dto.setRole(this.role);
        dto.setPhoneNumber(this.phoneNumber);
        dto.setBirthday(this.birthday);
        return dto;
    }

    @Override
    public UserEntity fromDto(UserDto dto) {
        this.firstName = dto.getFirstName();
        this.lastName = dto.getLastName();
        this.mail = dto.getMail();
        if (dto.getRole().equalsIgnoreCase("admin")) {
            this.role = UserRole.ADMIN.getRole();
        } else {
            this.role = UserRole.USER.getRole();
        }
        this.phoneNumber = dto.getPhoneNumber();
        this.birthday = dto.getBirthday();

        return this;
    }
}
