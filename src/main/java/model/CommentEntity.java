package model;

import dto.CommentDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Table(name = "Comment")
@Entity
public class CommentEntity extends BaseEntity<CommentDto> {
    @NotNull
    @Column(unique = true)
    private String key;
    private String description;
    @OneToOne
    private BoardUserEntity user;
    @OneToOne
    private TaskEntity task;

    @Override
    public CommentDto toDto() {
        CommentDto dto = new CommentDto();
        dto.setKey(key);
        dto.setDescription(description);
        if (null != user)
            dto.setUserEmail(user.getUser().getUser().getMail());
        if (null != task)
            dto.setTaskKey(this.task.key);
        return dto;
    }

    @Override
    public CommentEntity fromDto(CommentDto dto) {
        this.description = dto.getDescription();
        this.key = dto.getKey();
        return this;
    }
}
