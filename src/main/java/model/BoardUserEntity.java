package model;

import dto.BoardUserDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Getter
@Setter
@Table(name = "BoardUser")
@Entity
public class BoardUserEntity extends BaseEntity<BoardUserDto> {
    @OneToOne
    private ProjectUserEntity user;
    @OneToOne
    private BoardEntity board;

    @Override
    public BoardUserDto toDto() {
        BoardUserDto dto = new BoardUserDto();
        dto.setBoard(board.toDto());
        dto.setUser(user.toDto());
        return dto;
    }

    @Override
    public BoardUserEntity fromDto(BoardUserDto dto) {
        board = new BoardEntity().fromDto(dto.getBoard());

        return this;
    }
}
