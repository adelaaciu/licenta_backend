package model;

public enum ProjectUserRole {
    PRODUCT_OWNER("product_owner"),
    USER("user");

    private String projectRole;

    ProjectUserRole(String role) {
        this.projectRole = role;
    }

    public String getProjectRole() {
        return projectRole;
    }
}
