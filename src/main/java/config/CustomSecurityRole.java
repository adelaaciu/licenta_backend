package config;

import dto.BoardUserDto;
import dto.ProjectUserDto;
import dto.SprintDto;
import model.BoardEntity;
import model.ProjectUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import repository.BoardRepository;
import security.SecurityUtils;
import service.api.BoardUserService;
import service.api.ProjectUserService;
import service.api.SprintService;

@Component("customSecurityRole")
public class CustomSecurityRole {
    @Autowired
    private BoardUserService boardUserService;
    @Autowired
    private BoardRepository boardRepository;
    @Autowired
    private ProjectUserService projectUserService;
    @Autowired
    private SprintService sprintService;

    public boolean hasPMBoardPermissionForBacklog(final String backlogKey) {
        try {
            final BoardEntity board = boardRepository.findByBacklogKey(backlogKey);
            BoardUserDto user = boardUserService.getCurrentBoardUserPM(board.getKey());
            if (null != user && user.getUser().getProjectRole().equals(ProjectUserRole.PRODUCT_OWNER.getProjectRole())) {
//                System.out.println(user);
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean hasPMBoardPermissionForSprint(String sprintKey) {
        try {
            SprintDto sprint = sprintService.getSprint(sprintKey);
            final BoardEntity boardEntity = boardRepository.findByKey(sprint.getBoard().getKey());
            return hasPMBoardPermission(boardEntity.getKey());
        } catch (Exception e) {
            return false;
        }
    }

    public boolean hasPMProjectPermission(final String projectName) {
        try {
            ProjectUserDto projestUser = projectUserService.getProjectUserByUserMailAndProjectName(projectName);
            if (null != projestUser) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean hasPMBoardPermission(final String boardKey) {
        try {
            BoardUserDto boardUser = boardUserService.getCurrentBoardUserPM(boardKey);

            if (null != boardUser && boardUser.getUser().getProjectRole().equals(ProjectUserRole.PRODUCT_OWNER.getProjectRole())) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
