package service.api;

import dto.CommentDto;
import model.CommentEntity;

import java.util.List;

public interface DtosToEntitiesConvertor {
    List<CommentEntity> commentDtoListToEntityList(List<CommentDto> list, String boardKey);
}
