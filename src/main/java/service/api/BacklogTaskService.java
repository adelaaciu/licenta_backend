package service.api;

import dto.BacklogTaskDto;
import dto.SprintTaskDto;
import dto.TaskDto;
import model.BacklogTaskEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BacklogTaskService {

    BacklogTaskDto createTask(BacklogTaskDto task);

    BacklogTaskDto updateTask(BacklogTaskDto task, String boardKey);

    void removeTaskFromBacklog(String taskKey);

    List<BacklogTaskDto> getAllBacklogTasks(String backlogKey);


    BacklogTaskDto addTaskToBacklogFromSprint(String sprintKey, String taskKey);

    void removeAllTasksFromBacklog(String backlogKey);

    List<BacklogTaskDto> getBacklogByTitleContaining(String title);
}
