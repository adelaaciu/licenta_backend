package service.api;

import dto.BacklogDto;
import model.BacklogEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public interface BacklogService {
    BacklogDto createBacklog(String boardKey, BacklogDto backlog);

    BacklogDto getBacklog(String backlogKey);
}
