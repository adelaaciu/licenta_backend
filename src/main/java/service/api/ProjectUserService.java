package service.api;

import dto.ProjectUserDto;
import model.ProjectEntity;
import model.ProjectUserEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProjectUserService {

    List<ProjectUserDto> getProjectUsers(String projectName);

    List<ProjectUserDto> updateProjectUsers(String projectName, List<String> usersMail);

    List<ProjectUserDto> getAllByUserMail(String mail);

    void deleteProjectUsers(String projectName);

    void deleteProjectUser(ProjectEntity project, ProjectUserEntity projectUser);

    ProjectUserDto getProjectUserByUserMailAndProjectName(String projectName);

    ProjectUserEntity createAnonymousProjectUser(ProjectEntity project);

    ProjectUserDto addProjectOwner(String projectName, String pmMail);

    void deleteProjectUserByParams(String projectName, String userMail);
}
