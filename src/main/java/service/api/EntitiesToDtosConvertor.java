package service.api;

import dto.*;
import model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EntitiesToDtosConvertor {

    List<ProjectDto> convertProjectEntityToDtoList(List<ProjectEntity> list);

    List<ProjectUserDto> convertProjectUsersEntityToDtoList(List<ProjectUserEntity> users);

    List<BoardDto> convertBoardEntitiesToDtos(List<BoardEntity> boardEntities);

    List<BoardUserDto> convertBoardUserEntitiesToDtos(List<BoardUserEntity> users);

    List<SprintDto> convertSprintEntitiesToDtos(List<SprintEntity> sprints);

    List<SprintTaskDto> convertSprintTaskEntitiesToDtos(List<SprintTaskEntity> tasks);

    List<TaskDto> convertTaskEntitiesToDtos(List<TaskEntity> tasks);

    List<CommentDto> convertCommentEntityToDtoList(List<CommentEntity> comments);

    List<UserDto> convertUserEntityToDtoList(List<UserEntity> list);

    List<BacklogTaskDto> convertBacklogTaskEntitiesToDtod(List<BacklogTaskEntity> tasks);

    List<ReviewDto> convertReviewEntitiesToDtos(List<ReviewEntity> reviews);
}
