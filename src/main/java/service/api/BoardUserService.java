package service.api;

import dto.BoardUserDto;
import model.BoardEntity;
import model.BoardUserEntity;
import model.ProjectEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BoardUserService {

    List<BoardUserDto> updateBoardUsers(String boardKey, List<String> usersMail);

    List<BoardUserDto> getAllBoardUsers(String boardKey);

    BoardUserDto addWatcherToTask(String taskKey, BoardUserDto dto);

    BoardUserDto addAssigneeToTask(String taskKey, BoardUserDto dto);

    void deleteBoardUsers(String boardKey);

    BoardUserDto getCurrentBoardUser(String boardKey);

    void deleteBoardUser(String userMail, String boardKey);

    BoardUserEntity createAnonymousBoardUser(BoardEntity boardEntity, ProjectEntity project);

    BoardUserDto getCurrentBoardUserPM(String boardKey);
}
