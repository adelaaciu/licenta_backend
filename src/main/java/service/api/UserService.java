package service.api;

import dto.UserDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    List<UserDto> getAllUsers();

    List<UserDto> getUsersByName(String name);

    UserDto createUser(UserDto UserDto);

    UserDto updateUser(UserDto user);

    void deleteUserByEmail(String email);

    UserDto getCurrentUser();

    boolean isCurrentUserInRole(String role);

}
