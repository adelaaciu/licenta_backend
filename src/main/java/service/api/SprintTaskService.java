package service.api;

import dto.SprintTaskDto;
import model.SprintTaskEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SprintTaskService {
    SprintTaskDto addTaskToSprint(String sprintKey, String taskKey);

    List<SprintTaskDto> getAllSprintTasks(String sprintKey);

    SprintTaskDto updateTask(SprintTaskDto task, String boardKey);

    List<SprintTaskDto> getByTitleContaining(String title);

    List<SprintTaskDto> getUserTasks();

    void removeAllTasksFromSprint(String sprintKey);

    List<SprintTaskDto> getDoneTasks(String sprintKey);
}
