package service.api;

import dto.BoardDto;
import model.BoardEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BoardService {
    List<BoardDto> getProjectBoards(String projectName);

    BoardDto getBoard(String key);

    BoardDto createBoard(BoardDto dto);

    void deleteProjectBoard(String projectName);

    void deleteBoard(String key);
}

