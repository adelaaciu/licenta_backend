package service.api;

import dto.SprintDto;
import model.SprintEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SprintService {
    SprintDto createSprint(SprintDto dto);

    List<SprintDto> getBoardSprints(String boardKey);

    SprintDto endSprint(String sprintKey);
    SprintDto startSprint(String sprintKey, int sprintDays);

    SprintDto getSprint(String sprintKey);

    void deleteSprint(String sprintKey);

    SprintDto getActiveSprint(String boardKey);
}
