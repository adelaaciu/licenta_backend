package service.api;

import dto.CommentDto;
import model.BoardEntity;
import model.ProjectEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {
    CommentDto addComment(CommentDto comment);

    List<CommentDto> getComments(String taskKey);

    void deleteUserComment(String userMail, BoardEntity boardEntity, ProjectEntity project);
}
