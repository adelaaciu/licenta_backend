package service.api;

import dto.DashboardDto;
import dto.TaskDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TaskService {
    TaskDto changePriority(String taskKey, String priority);

    TaskDto updateTaskStatus(String taskKey, String status);

    TaskDto addStoryPointsToTask(String taskKey, int storyPoints);

    List<DashboardDto> getLatestUpdates();

    DashboardDto getDashboardDto(TaskDto task);
}
