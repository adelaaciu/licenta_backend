package service.api;

import dto.ProjectDto;
import model.ProjectEntity;
import model.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProjectService {
    List<ProjectDto> getAllProjects();

    List<ProjectDto> getProjectsByNameContaining(String name);

    ProjectDto createProject(ProjectDto projectDTO);

    void deleteProject(String name);

    List<ProjectDto> getProjectsByUser();

    ProjectDto getProjectsByName(String name);
}
