package service.api;

import dto.ReviewDto;

import java.util.List;

public interface ReviewService {
    ReviewDto addReview(ReviewDto dto);

    List<ReviewDto> getSprintReviewsByCategory(String sprintKey, String category);

    void deleteReviews(String sprintKey);
}
