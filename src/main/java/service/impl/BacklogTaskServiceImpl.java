package service.impl;

import dto.BacklogTaskDto;
import exception.task.BacklogTaskAlreadyExistException;
import exception.task.BacklogTaskNotFoundException;
import exception.sprint.SprintNotFoundException;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import repository.*;
import service.api.BacklogTaskService;
import service.api.DtosToEntitiesConvertor;
import service.api.EntitiesToDtosConvertor;

import java.util.*;

@Service
public class BacklogTaskServiceImpl implements BacklogTaskService {
    @Autowired
    private BacklogTaskRepository backlogTaskRepository;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;
    @Autowired
    private SprintTaskRepository sprintTaskRepository;
    @Autowired
    private SprintRepository sprintRepository;
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private DtosToEntitiesConvertor dtosToEntitiesConvertor;
    @Autowired
    private BoardUserRepository boardUserRepository;
    @Autowired
    private MailService mailService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BoardRepository boardRepository;

    @Override
//    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @PreAuthorize("@customSecurityRole.hasPMBoardPermissionForBacklog(#task.getBacklog().getKey())")
    public BacklogTaskDto createTask(BacklogTaskDto task) {
        if (backlogTaskRepository.findByTitle(task.getTitle()) != null) {
            throw new BacklogTaskAlreadyExistException();
        }
        BacklogTaskEntity backlogTaskEntity = new BacklogTaskEntity().fromDto(task);
        backlogTaskEntity.setActualizationDate(new GregorianCalendar().getTime());
        backlogTaskEntity.setComments(new ArrayList<>());

        BacklogEntity backlogEntity = backlogRepository.findByKey(task.getBacklog().getKey());
        backlogTaskEntity.setBacklog(backlogEntity);
        List<BacklogTaskEntity> backlogTaskList = backlogTaskRepository.findAllByBacklogKey(backlogEntity.getKey());
        backlogEntity.setTasks(backlogTaskList);
        backlogTaskEntity.setKey(backlogEntity.getKey() + "-Task-" + UUID.randomUUID() + 1);
        backlogRepository.save(backlogEntity);

        backlogTaskEntity = backlogTaskRepository.save(backlogTaskEntity);

        return backlogTaskEntity.toDto();
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public BacklogTaskDto updateTask(BacklogTaskDto task, String boardKey) {
        boolean sentMail = false;
        BacklogTaskEntity taskEntity = backlogTaskRepository.findByKey(task.getKey());
        if (taskEntity == null) {
            throw new BacklogTaskNotFoundException();
        }

        task.setActualizationDate(new GregorianCalendar().getTime());

        if (taskEntity.getStatus().equals(Status.TODO.getMessage()) && task.getStatus().equals(Status.DEVIMPL.getMessage())) {
            task.setStartDate(new GregorianCalendar().getTime());
        }

        if (!taskEntity.getStatus().equals(Status.DONE.getMessage()) && task.getStatus().equals(Status.DONE.getMessage())) {
            task.setEndDate(new GregorianCalendar().getTime());
        }

        taskEntity = taskEntity.fromDto(task);
        if (null != task.getWatcher()) {
            taskEntity.setWatcher(boardUserRepository.findByUserUserMailAndBoardKey(task.getWatcher().getUser().getUser().getMail(), boardKey));
        }
        if (null != task.getAssignee()) {
            if(taskEntity.getAssignee() == null || !taskEntity.getAssignee().getUser().getUser().getMail().equals(task.getAssignee().getUser().getUser().getMail())) {
                sentMail = true;
                UserEntity userEntity = userRepository.findByMail(task.getAssignee().getUser().getUser().getMail());
//                mailService.sendEmailTaskAssigned(userEntity, MailTemplate.ASSIGN_TASK, new Context(), task);
            }
            taskEntity.setAssignee(boardUserRepository.findByUserUserMailAndBoardKey(task.getAssignee().getUser().getUser().getMail(), boardKey));
        }

        taskEntity.setBacklog(backlogRepository.findByKey(task.getBacklog().getKey()));

        BoardEntity board = boardRepository.findByBacklogKey(task.getBacklog().getKey());
        List<CommentEntity> comments = dtosToEntitiesConvertor.commentDtoListToEntityList(task.getComments(), board.getKey());


        comments.forEach(comment -> {
            if (comment.getKey() == null) {
                comment.setKey(comment.getTask().getKey() + "-Comm-" + commentRepository.findAllByTaskKey(comment.getTask().getKey()).size() + 1);
                commentRepository.save(comment);
            }
        });

        taskEntity.setComments(commentRepository.findAllByTaskKey(task.getKey()));

        backlogTaskRepository.save(taskEntity);

        if(!sentMail && task.getAssignee() != null) {
            UserEntity userEntity = userRepository.findByMail(task.getAssignee().getUser().getUser().getMail());
//            mailService.sendEmailTaskUpdated(userEntity, MailTemplate.UPDATE_TASK, new Context(), task);
        }

        return task;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void removeTaskFromBacklog(String taskKey) {
        BacklogTaskEntity task = backlogTaskRepository.findByKey(taskKey);
        if (task == null) {
            throw new BacklogTaskNotFoundException();
        }
        BacklogEntity backlogEntity = backlogRepository.findByKey(task.getBacklog().getKey());
        backlogEntity.getTasks().remove(task);
        backlogRepository.save(backlogEntity);

        task.setComments(new ArrayList<>());
        task.setAssignee(null);
        task.setWatcher(null);
        List<CommentEntity> commentEntities = commentRepository.findAllByTaskKey(taskKey);
        commentEntities.forEach(x -> {
            x.setUser(null);
            x.setTask(null);
            commentRepository.delete(x);
        });
        backlogTaskRepository.delete(task.getId());
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public BacklogTaskDto addTaskToBacklogFromSprint(String sprintKey, String taskKey) {
        SprintTaskEntity sprintTaskEntity = sprintTaskRepository.findByKey(taskKey);
        SprintEntity sprintEntity = sprintRepository.findByKey(sprintKey);
        BacklogEntity backlogEntity = backlogRepository.findByKey(sprintEntity.getBoard().getBacklog().getKey());

        if (sprintEntity == null) {
            throw new SprintNotFoundException();
        }

        BacklogTaskEntity backlogTaskEntity = new BacklogTaskEntity();
        backlogTaskEntity.setBacklog(backlogEntity);

        System.out.println(sprintTaskEntity);

        if (sprintTaskEntity.getAssignee() != null)
            backlogTaskEntity.setAssignee(sprintTaskEntity.getAssignee());
        if (sprintTaskEntity.getWatcher() != null)
            backlogTaskEntity.setWatcher(sprintTaskEntity.getWatcher());
        if (sprintTaskEntity.getActualizationDate() != null)
            backlogTaskEntity.setActualizationDate(sprintTaskEntity.getActualizationDate());
        backlogTaskEntity.setTitle(sprintTaskEntity.getTitle());
        backlogTaskEntity.setDescription(sprintTaskEntity.getDescription());
        backlogTaskEntity.setStatus(sprintTaskEntity.getStatus());
        if (sprintTaskEntity.getStartDate() != null)
            backlogTaskEntity.setStartDate(sprintTaskEntity.getStartDate());
        if (sprintTaskEntity.getEndDate() != null)
            backlogTaskEntity.setEndDate(sprintTaskEntity.getEndDate());
        backlogTaskEntity.setStoryPoints(sprintTaskEntity.getStoryPoints());
        backlogTaskEntity.setPriority(sprintTaskEntity.getPriority());
        backlogTaskEntity.setKey(taskKey);

        List<CommentEntity> comments = new ArrayList<>();

        List<CommentEntity> commentEntities = commentRepository.findAllByTaskKey(taskKey);

        sprintTaskEntity.setComments(new ArrayList<>());
        sprintTaskEntity = sprintTaskRepository.save(sprintTaskEntity);
        sprintEntity.getTasks().remove(sprintTaskEntity);

        commentEntities.forEach(x -> {
            CommentEntity comment = new CommentEntity();
            comment.setUser(x.getUser());
            comment.setDescription(x.getDescription());
            comments.add(comment);

            x.setUser(null);
            x.setTask(null);
            x = commentRepository.save(x);
            commentRepository.delete(x.getId());
        });

        sprintRepository.save(sprintEntity);
        sprintTaskRepository.delete(sprintTaskEntity);
        backlogTaskEntity = backlogTaskRepository.save(backlogTaskEntity);
        BacklogTaskEntity finalBacklogTaskEntity = backlogTaskEntity;

        comments.forEach(commentEntity -> {
            commentEntity.setTask(finalBacklogTaskEntity);
            commentEntity.setKey(taskKey + "-Comm-" + commentRepository.findAllByTaskKey(commentEntity.getTask().getKey()).size() + 1);
            commentRepository.save(commentEntity);
        });


        backlogTaskEntity.setComments(comments);
        backlogTaskEntity = backlogTaskRepository.save(backlogTaskEntity);
        backlogEntity.getTasks().add(backlogTaskEntity);

        BacklogTaskDto backlogTaskDto = backlogTaskEntity.toDto();
        backlogTaskDto.setComments(entitiesToDtosConvertor.convertCommentEntityToDtoList(commentRepository.findAllByTaskKey(taskKey)));

        return backlogTaskDto;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void removeAllTasksFromBacklog(String backlogKey) {
        List<BacklogTaskEntity> taskList = backlogTaskRepository.findAllByBacklogKey(backlogKey);

        for (BacklogTaskEntity task : taskList) {
            this.removeTaskFromBacklog(task.getKey());
        }

    }


    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<BacklogTaskDto> getBacklogByTitleContaining(String title) {
        List<BacklogTaskEntity> list = backlogTaskRepository.findAllByTitleContainingIgnoringCase(title);
        if (null == list) {
            throw new BacklogTaskNotFoundException();
        }
        return entitiesToDtosConvertor.convertBacklogTaskEntitiesToDtod(list);
    }


    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<BacklogTaskDto> getAllBacklogTasks(String backlogKey) {
        List<BacklogTaskEntity> list = backlogTaskRepository.findAllByBacklogKey(backlogKey);
        if (null == list) {
            throw new BacklogTaskNotFoundException();
        }
        return entitiesToDtosConvertor.convertBacklogTaskEntitiesToDtod(list);
    }
}
