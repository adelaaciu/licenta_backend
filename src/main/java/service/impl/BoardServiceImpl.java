package service.impl;

import dto.BacklogTaskDto;
import dto.BoardDto;
import exception.board.BoardAlreadyExistException;
import exception.board.BoardNotFoundException;
import exception.project.ProjectNotFoundException;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repository.*;
import security.SecurityUtils;
import service.api.*;

import java.util.ArrayList;
import java.util.List;

@Service
public class BoardServiceImpl implements BoardService {
    @Autowired
    private BoardRepository boardRepository;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private SprintRepository sprintRepository;
    @Autowired
    private BoardUserRepository boardUserRepository;
    @Autowired
    private ProjectUserRepository projectUserRepository;
    @Autowired
    private BoardUserService boardUserService;
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private BacklogTaskService backlogTaskService;
    @Autowired
    private SprintService sprintService;

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<BoardDto> getProjectBoards(String projectName) {
        List<BoardEntity> boardEntities = boardRepository.findAllByProjectName(projectName);
        String mail = SecurityUtils.getCurrentUserLogin();
//        BoardUserEntity currentBoardUser = boardUserRepository.findByUserUserMailAndBoardProjectName(mail, projectName);
        ProjectUserEntity projectUserEntity = projectUserRepository.findByUserMailAndProjectName(mail, projectName);

        if (null == boardEntities) {
            throw new BoardNotFoundException();
        }

        if(null != projectUserEntity && projectUserEntity.getProjectRole().equalsIgnoreCase(ProjectUserRole.USER.getProjectRole())) {
            List<BoardEntity> userBoards = new ArrayList<>();
            boardEntities.forEach(board -> {

                boolean exist = false;
                for (BoardUserEntity boardUser : board.getBoardUsers()) {
                    if (boardUser.getUser().equals(projectUserEntity)) {
                        exist = true;
                    }
                }

                if(exist) {
                    userBoards.add(board);
                }
            });

            return entitiesToDtosConvertor.convertBoardEntitiesToDtos(userBoards);
        }

        return entitiesToDtosConvertor.convertBoardEntitiesToDtos(boardEntities);

    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public BoardDto getBoard(String key) {
        BoardEntity board = boardRepository.findByKey(key);
        if (null == board) {
            throw new BoardNotFoundException();
        }

        BoardDto boardDto = board.toDto();
        boardDto.setSprints(entitiesToDtosConvertor.convertSprintEntitiesToDtos(sprintRepository.findAllByBoardKey(board.getKey())));
        boardDto.setBoardUsers(entitiesToDtosConvertor.convertBoardUserEntitiesToDtos(boardUserRepository.findAllByBoardKey(board.getKey())));
        return boardDto;
    }

    @Override
//    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @PreAuthorize("@customSecurityRole.hasPMProjectPermission(#dto.getProject().getName())")
    public BoardDto createBoard(BoardDto dto) {
        ProjectEntity projectEntity = projectRepository.findByName(dto.getProject().getName());
        if (projectEntity == null) {
            throw new ProjectNotFoundException();
        }

        List<BoardEntity> boardEntityList = boardRepository.findAllByProjectName(dto.getProject().getName());

        BoardEntity boardEntity = new BoardEntity().fromDto(dto);
        boardEntity.setKey(projectEntity.getName() + "-Board-V" + (boardEntityList.size() + 1));
        boardEntity.setSprints(new ArrayList<>());
        boardEntity.setBoardUsers(new ArrayList<>());
        boardEntity.setProject(projectEntity);
        BacklogEntity backlogEntity = new BacklogEntity();
        backlogEntity.setKey(boardEntity.getKey() + "-Backlog");
        backlogRepository.save(backlogEntity);
        boardEntity.setBacklog(backlogEntity);
        boardEntity = boardRepository.save(boardEntity);


        List<ProjectUserEntity> projectUsers = projectUserRepository.findAllByProjectName(dto.getProject().getName());
        projectEntity.setUsers(projectUsers);
        projectEntity = projectRepository.save(projectEntity);



        for (ProjectUserEntity projectUserEntity : projectUsers) {
            if (projectUserEntity.getProjectRole().equals(ProjectUserRole.PRODUCT_OWNER.getProjectRole())) {
                BoardUserEntity boardUserEntity = new BoardUserEntity();
                boardUserEntity.setUser(projectUserEntity);
                boardUserEntity.setBoard(boardEntity);
                boardUserRepository.save(boardUserEntity);
            }
        }

        boardEntity.setBoardUsers(boardUserRepository.findAllByBoardKey(boardEntity.getKey()));
        boardEntity = boardRepository.save(boardEntity);

        boardEntityList.add(boardEntity);
        projectEntity.setBoards(boardEntityList);

        BoardDto boardDto = boardEntity.toDto();
        boardDto.setBoardUsers(entitiesToDtosConvertor.convertBoardUserEntitiesToDtos(boardUserRepository.findAllByBoardKey(boardDto.getKey())));
        boardDto.setSprints(new ArrayList<>());
        return boardDto;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
//    @PreAuthorize("@customSecurityRole.hasPMProjectPermission(projectName)")
    public void deleteProjectBoard(String projectName) {
        List<BoardEntity> boards = boardRepository.findAllByProjectName(projectName);

        boards.forEach(board -> {
            deleteBoard(board.getKey());
        });


    }

    //    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @PreAuthorize("@customSecurityRole.hasPMBoardPermission(#boardKey)")
    public void deleteBoard(String boardKey) {
        BoardEntity board = boardRepository.findByKey(boardKey);
        if (board == null) {
            throw new BoardNotFoundException();
        }

        boardUserRepository.findAll();
        boardUserRepository.findAllByBoardKey(boardKey);
        board.getBoardUsers();

        String projectName = board.getProject().getName();
        ProjectEntity projectEntity = projectRepository.findByName(projectName);

        projectEntity.getBoards().remove(board);
        projectRepository.save(projectEntity);

        List<SprintEntity> sprints = sprintRepository.findAllByBoardKey(boardKey);
        for (SprintEntity sprint : sprints) {
             sprintService.deleteSprint(sprint.getKey());
        }
        backlogTaskService.removeAllTasksFromBacklog(board.getBacklog().getKey());
        Long backlogId = board.getBacklog().getId();
        board.setBacklog(null);
        backlogRepository.delete(backlogId);
        boardUserService.deleteBoardUsers(board.getKey());
        board.setBoardUsers(new ArrayList<>());
        board.setSprints(new ArrayList<>());
        board.setProject(null);
        boardRepository.delete(board.getId());

        projectEntity.setBoards(boardRepository.findAllByProjectName(projectName));
        projectRepository.save(projectEntity);
    }

}
