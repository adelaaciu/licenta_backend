package service.impl;

import dto.*;
import exception.mail.MailContentNotFoundException;
import model.UserEntity;
import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import security.ApplicationProperties;
import service.api.TaskService;

import javax.mail.internet.MimeMessage;
import java.util.Locale;

/**
 * Service for sending e-mails.
 * <p>
 * We use the @Async annotation to send e-mails asynchronously.
 * </p>
 */
@Service
public class MailService {
    private final Logger log = LoggerFactory.getLogger(MailService.class);
    static final String EXTERNAL_IP_ADDRESS = "External ip address: ";
    final ApplicationProperties applicationProperties;
    final MessageSource messageSource;
    final Environment env;
    final JavaMailSender javaMailSender;
    final SpringTemplateEngine templateEngine;
    @Autowired
    private TaskService taskService;

    @Autowired
    public MailService(MessageSource messageSource, JavaMailSender javaMailSender, Environment env, ApplicationProperties applicationProperties, SpringTemplateEngine templateEngine) {
        this.messageSource = messageSource;
        this.env = env;
        this.javaMailSender = javaMailSender;
        this.applicationProperties = applicationProperties;
        this.templateEngine = templateEngine;
    }

    @Async
    void sendEmailTaskAssigned(UserEntity to, MailTemplate template, Context context, TaskDto task) {
        if (to == null || to.getMail() == null || to.getMail().isEmpty())
            return;
        log.debug("Sending generic e-mail to '{}'", to.getMail());
//        String langKey = "en";

        Locale locale = new Locale("en");
        context.setLocale(locale);
        context.setVariable("applicationName", "Think-agile");
        context.setVariable("pageTitle", "Assign Task");
        context.setVariable("message", "Task " + task.getTitle() + " was assign to you");
        DashboardDto dashboard = taskService.getDashboardDto(task);
        String taskLink = "http://localhost:4200/";
        if (task instanceof SprintTaskDto) {
            taskLink += this.getSprintTaskLink(dashboard);
        } else {
            taskLink += this.getBacklogTaskLink(dashboard);
        }
        context.setVariable("taskLink", taskLink);

        String content = templateEngine.process(template.toString(), context);
        if (content.isEmpty())
            throw new MailContentNotFoundException();

        String subject = template.getSubjectKey();
        if (subject.isEmpty())
            throw new MailContentNotFoundException();

        sendEmail("adelaaciu@gmail.com", subject, content, false, true);
//        sendEmail(to.getMail(), subject, content, false, true);
    }


    @Async
    void sendEmailProjectAssigned(UserEntity to, MailTemplate template, Context context, ProjectDto projectDto) {
        if (to == null || to.getMail() == null || to.getMail().isEmpty())
            return;
        log.debug("Sending generic e-mail to '{}'", to.getMail());
        Locale locale = new Locale("en");
        context.setLocale(locale);
        context.setVariable("applicationName", "Think-agile");
        context.setVariable("pageTitle", "New project");
        context.setVariable("message", "You have a new project: " + projectDto.getName());

        String projectLink = "http://localhost:4200/user/projects/" + projectDto.getName() + "/boards";

        context.setVariable("projectLink", projectLink);

        String content = templateEngine.process(template.toString(), context);
        if (content.isEmpty())
            throw new MailContentNotFoundException();

        String subject = template.getSubjectKey();
        if (subject.isEmpty())
            throw new MailContentNotFoundException();

        sendEmail("adelaaciu@gmail.com", subject, content, false, true);
//        sendEmail(to.getMail(), subject, content, false, true);
    }

    @Async
    void sendEmailBoardAssigned(UserEntity to, MailTemplate template, Context context, BoardDto boardDto) {
        if (to == null || to.getMail() == null || to.getMail().isEmpty())
            return;
        log.debug("Sending generic e-mail to '{}'", to.getMail());

        Locale locale = new Locale("en");
        context.setLocale(locale);
        context.setVariable("applicationName", "Think-agile");
        context.setVariable("pageTitle", "New board added for you");
        context.setVariable("message", "You are assign to mew board " + boardDto.getName());

        String boardLink = "http://localhost:4200/user/projects/" + boardDto.getProject().getName() + "/boards/" + boardDto.getKey() + "/items";

        context.setVariable("boardLink", boardLink);

        String content = templateEngine.process(template.toString(), context);
        if (content.isEmpty())
            throw new MailContentNotFoundException();

        String subject = template.getSubjectKey();
        if (subject.isEmpty())
            throw new MailContentNotFoundException();

        sendEmail("adelaaciu@gmail.com", subject, content, false, true);
//        sendEmail(to.getMail(), subject, content, false, true);
    }


    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}'.", isMultipart, isHtml, to,
                subject);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setTo(to);
            message.setFrom("thinkagileadm@gmail.com"); // **** create email address
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent e-mail to UserEntity '{}'", to);
        } catch (Exception e) {
            log.warn("E-mail could not be sent to user '{}'", to, e);
        }

    }


    public String getBacklogTaskLink(DashboardDto dashboard) {
        return "user/projects/" + dashboard.getProjectDto().getName()
                + "/boards/" + dashboard.getBoardDto().getKey() + "/backlog/" + dashboard.getBacklogDto().getKey();
    }

    public String getSprintTaskLink(DashboardDto dashboard) {
        return "user/projects/" + dashboard.getProjectDto().getName()
                + "/boards/" + dashboard.getBoardDto().getKey() + "/sprints/" + dashboard.getSprintDto().getKey();
    }

    public void sendEmailTaskUpdated(UserEntity to, MailTemplate template, Context context, TaskDto task) {
        if (to == null || to.getMail() == null || to.getMail().isEmpty())
            return;
        log.debug("Sending generic e-mail to '{}'", to.getMail());

        Locale locale = new Locale("en");
        context.setLocale(locale);
        context.setVariable("applicationName", "Think-agile");
        context.setVariable("pageTitle", "Update Task");
        context.setVariable("message", "Task " + task.getTitle() + " was updated");
        DashboardDto dashboard = taskService.getDashboardDto(task);
        String taskLink = "http://localhost:4200/";
        if (task instanceof SprintTaskDto) {
            taskLink += this.getSprintTaskLink(dashboard);
        } else {
            taskLink += this.getBacklogTaskLink(dashboard);
        }
        context.setVariable("taskLink", taskLink);

        String content = templateEngine.process(template.toString(), context);
        if (content.isEmpty())
            throw new MailContentNotFoundException();

        String subject = template.getSubjectKey();
        if (subject.isEmpty())
            throw new MailContentNotFoundException();

        sendEmail("adelaaciu@gmail.com", subject, content, false, true);
//        sendEmail(to.getMail(), subject, content, false, true);
    }
}