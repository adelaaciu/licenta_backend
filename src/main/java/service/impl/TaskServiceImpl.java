package service.impl;

import dto.BacklogTaskDto;
import dto.DashboardDto;
import dto.SprintTaskDto;
import dto.TaskDto;
import exception.task.TaskNotFoundException;
import model.TaskEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import repository.TaskRepository;
import security.SecurityUtils;
import service.api.EntitiesToDtosConvertor;
import service.api.TaskService;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public TaskDto changePriority(String taskKey, String priority) {
        TaskEntity task = taskRepository.findByKey(taskKey);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setPriority(priority);

        TaskDto taskDto = task.toDto();
        return taskDto;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public TaskDto updateTaskStatus(String taskKey, String status) {
        TaskEntity task = taskRepository.findByKey(taskKey);
        if (task == null) {
            throw new TaskNotFoundException();
        }

        task.setActualizationDate(new GregorianCalendar().getTime());
        task.setStatus(status);
        taskRepository.save(task);
        TaskDto taskDto = task.toDto();
        return taskDto;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public TaskDto addStoryPointsToTask(String taskKey, int storyPoints) {
        TaskEntity task = taskRepository.findByKey(taskKey);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setStoryPoints(storyPoints);
        task.setActualizationDate(new GregorianCalendar().getTime());
        TaskDto taskDto = task.toDto();
        return taskDto;
    }

    @Override
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public List<DashboardDto> getLatestUpdates() {
        String mail = SecurityUtils.getCurrentUserLogin();
        List<TaskEntity> tasks = taskRepository.findTop10ByAssigneeUserUserMailOrWatcherUserUserMailOrderByActualizationDate(mail, mail);

        if (tasks.isEmpty() || tasks == null) {
            throw new TaskNotFoundException();
        }

        List<TaskDto> taskDtos = entitiesToDtosConvertor.convertTaskEntitiesToDtos(tasks);
        List<DashboardDto> dashboardDtos = new ArrayList<>();


        taskDtos.forEach(task -> {
            DashboardDto dashboardDto = getDashboardDto(task);

            dashboardDtos.add(dashboardDto);
        });

        return dashboardDtos;
    }

    public DashboardDto getDashboardDto(TaskDto task) {
        DashboardDto dashboardDto = new DashboardDto();
        dashboardDto.setTaskDto(task);
        if (task.getWatcher() != null) {
            dashboardDto.setProjectDto(task.getWatcher().getBoard().getProject());
            dashboardDto.setBoardDto(task.getWatcher().getBoard());
        } else {
            if (task.getAssignee() != null) {
                dashboardDto.setProjectDto(task.getAssignee().getBoard().getProject());
                dashboardDto.setBoardDto(task.getAssignee().getBoard());
            }
        }

        if(task instanceof BacklogTaskDto) {
            dashboardDto.setBacklogDto(((BacklogTaskDto)task).getBacklog());
        } else {
            dashboardDto.setSprintDto(((SprintTaskDto)task).getSprint());
        }
        return dashboardDto;
    }
}
