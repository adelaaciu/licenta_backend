package service.impl;

import dto.CommentDto;
import model.CommentEntity;
import model.TaskEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.BoardUserRepository;
import repository.TaskRepository;
import service.api.DtosToEntitiesConvertor;

import java.util.ArrayList;
import java.util.List;

@Service
public class DtosToEntitiesConvertorImpl implements DtosToEntitiesConvertor {
    @Autowired
    private BoardUserRepository boardUserRepository;
    @Autowired
    private TaskRepository taskRepository;

    @Override
    public List<CommentEntity> commentDtoListToEntityList(List<CommentDto> list, String boardKey) {
        List<CommentEntity> commentEntities = new ArrayList<>();
        list.forEach(commentDto -> {
            CommentEntity commentEntity = new CommentEntity().fromDto(commentDto);
            commentEntity.setUser(boardUserRepository.findByUserUserMailAndBoardKey(commentDto.getUserEmail(), boardKey));
            TaskEntity taskEntity = taskRepository.findByKey(commentDto.getTaskKey());
            commentEntity.setTask(taskEntity);
            commentEntities.add(commentEntity);
        });
        return commentEntities;
    }
}
