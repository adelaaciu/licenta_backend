package service.impl;

import dto.SprintDto;
import exception.sprint.SprintNotFoundException;
import model.BoardEntity;
import model.SprintEntity;
import model.SprintTaskEntity;
import model.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import repository.BoardRepository;
import repository.CommentRepository;
import repository.SprintRepository;
import repository.SprintTaskRepository;
import service.api.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

@Service
public class SprintServiceImpl implements SprintService {
    @Autowired
    private SprintRepository sprintRepository;
    @Autowired
    private BoardRepository boardRepository;
    @Autowired
    private SprintTaskService sprintTaskService;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;
    @Autowired
    private BacklogTaskService backlogTaskService;

    @Autowired
    private SprintTaskRepository sprintTaskRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private ReviewService reviewService;

    @Override
    @PreAuthorize("@customSecurityRole.hasPMBoardPermission(#dto.getBoard().getKey())")
    public SprintDto createSprint(SprintDto dto) {
        final BoardEntity boardEntity = boardRepository.findByKey(dto.getBoard().getKey());
        int size = sprintRepository.findAllByBoardKey(boardEntity.getKey()).size();
        dto.setKey(boardEntity.getKey() + "-Sprint-" + (size + 1));
        dto.setSprintNo(size + 1);
        SprintEntity sprintEntity = new SprintEntity().fromDto(dto);
        sprintEntity.setBoard(boardEntity);
        sprintEntity.setTasks(new ArrayList<>());
        sprintEntity.setClosed(true);
        sprintEntity = sprintRepository.save(sprintEntity);

        if (boardEntity.getSprints() == null) {
            boardEntity.setSprints(new ArrayList<>());
        } else {
            boardEntity.setSprints(sprintRepository.findAllByBoardKey(boardEntity.getKey()));
        }

        final SprintDto sprintDto = sprintEntity.toDto();
        sprintDto.setTasks(new ArrayList<>());
        return sprintDto;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<SprintDto> getBoardSprints(String boardKey) {
        return null;
    }

    @Override
//    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @PreAuthorize("@customSecurityRole.hasPMBoardPermissionForSprint(#sprintKey)")
    public SprintDto endSprint(String sprintKey) {
        SprintEntity sprint = sprintRepository.findByKey(sprintKey);
        if (null == sprint) {
            throw new SprintNotFoundException();
        }

        sprint.setEndDate(new GregorianCalendar().getTime());
        sprint.setClosed(true);

        List<SprintTaskEntity> tasks = sprintTaskRepository.findAllBySprintKey(sprintKey);
        if (tasks != null) {
            tasks.forEach(x -> {
                if (!x.getStatus().equals(Status.DONE.getMessage())) {
                    backlogTaskService.addTaskToBacklogFromSprint(sprintKey, x.getKey());
                }
            });
        }

        sprint.setTasks(new ArrayList<>());
        sprint = sprintRepository.save(sprint);

        SprintDto sprintDto = sprint.toDto();
        sprintDto.setTasks(entitiesToDtosConvertor.convertSprintTaskEntitiesToDtos(sprint.getTasks()));

        return sprintDto;
    }

    @Override
//    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @PreAuthorize("@customSecurityRole.hasPMBoardPermissionForSprint(#sprintKey)")
    public SprintDto startSprint(String sprintKey, int sprintDays) {
        SprintEntity sprint = sprintRepository.findByKey(sprintKey);
        if (null == sprint) {
            throw new SprintNotFoundException();
        }

        sprint.setStartDate(new GregorianCalendar().getTime());
        sprint.setClosed(false);
        sprint.setDays(sprintDays);
        sprint = sprintRepository.save(sprint);

        SprintDto sprintDto = sprint.toDto();
        sprintDto.setTasks(entitiesToDtosConvertor.convertSprintTaskEntitiesToDtos(sprint.getTasks()));

        return sprintDto;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public SprintDto getSprint(String sprintKey) {
        SprintEntity sprintEntity = sprintRepository.findByKey(sprintKey);
        if (sprintEntity == null) {
            throw new SprintNotFoundException();
        }
        return sprintEntity.toDto();
    }

    @Override
//    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @PreAuthorize("@customSecurityRole.hasPMBoardPermissionForSprint(#sprintKey)")
    public void deleteSprint(String sprintKey) {
        SprintEntity sprintEntity = sprintRepository.findByKey(sprintKey);
        if (sprintEntity == null) {
            throw new SprintNotFoundException();
        }

        List<SprintTaskEntity> tasks = sprintTaskRepository.findAllBySprintKey(sprintKey);
        if (tasks != null) {
            tasks.forEach(x -> {
                if (x.getStatus() != Status.DONE.getMessage()) {
                    backlogTaskService.addTaskToBacklogFromSprint(sprintKey, x.getKey());
                }
            });
        }

        sprintEntity.setTasks(new ArrayList<>());

        reviewService.deleteReviews(sprintKey);

        sprintRepository.delete(sprintEntity.getId());
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public SprintDto getActiveSprint(String boardKey) {
        List<SprintEntity> list = sprintRepository.findAllByBoardKey(boardKey);
        SprintEntity activeSprint = null;
        for (SprintEntity sprint : list) {
            if (!sprint.isClosed()) {
                activeSprint = sprint;
                break;
            }
        }
        if (null == activeSprint) {
            return null;
        }
        return activeSprint.toDto();
    }
}
