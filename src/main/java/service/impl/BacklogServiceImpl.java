package service.impl;

import dto.BacklogDto;
import exception.backlog.BacklogAlreadyExistException;
import exception.backlog.BacklogNotFoundException;
import model.BacklogEntity;
import model.BoardEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import repository.BacklogRepository;
import repository.BoardRepository;
import service.api.BacklogService;

import java.util.ArrayList;

@Service
public class BacklogServiceImpl implements BacklogService {
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private EntitiesToDtosConvertorImpl entitiesToDtosConvertor;
    @Autowired
    private BoardRepository boardRepository;

    // unused in UI
    @Override
    @PreAuthorize("hasRole('ROLE_USER')")
    public BacklogDto createBacklog(String boardKey, BacklogDto backlog) {
        if (backlogRepository.findByKey(boardKey) != null) {
            throw new BacklogAlreadyExistException();
        }
        BacklogEntity backlogEntity = new BacklogEntity().fromDto(backlog);
        backlogEntity.setTasks(new ArrayList<>());
        backlogEntity = backlogRepository.save(backlogEntity);

        BoardEntity boardEntity = boardRepository.findByKey(boardKey);
        boardEntity.setBacklog(backlogEntity);
        boardRepository.save(boardEntity);

        return backlogEntity.toDto();
    }

    @Override
    @PreAuthorize("hasRole('ROLE_USER')")
    public BacklogDto getBacklog(String backlogKey) {
        BacklogEntity backlog = backlogRepository.findByKey(backlogKey);
        if (backlog == null) {
            throw new BacklogNotFoundException();
        }

        BacklogDto backlogDto = backlog.toDto();
        backlogDto.setTasks(entitiesToDtosConvertor.convertBacklogTaskEntitiesToDtod(backlog.getTasks()));
        
        return backlogDto;
    }
}
