package service.impl;

import dto.ReviewDto;
import model.BoardUserEntity;
import model.ReviewEntity;
import model.SprintEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.BoardUserRepository;
import repository.ReviewRepository;
import repository.SprintRepository;
import service.api.BoardUserService;
import service.api.EntitiesToDtosConvertor;
import service.api.ReviewService;
import service.api.SprintService;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;
    @Autowired
    private SprintRepository sprintRepository;
    @Autowired
    private BoardUserRepository boardUserRepository;


    @Override
    public ReviewDto addReview(ReviewDto dto) {
        ReviewEntity reviewEntity = new ReviewEntity().fromDto(dto);
        final SprintEntity sprint = this.sprintRepository.findByKey(dto.getSprint().getKey());
        final BoardUserEntity user = this.boardUserRepository.findByUserUserMailAndBoardKey(dto.getUser().getUser().getUser().getMail(), dto.getUser().getBoard().getKey());

        reviewEntity.setSprint(sprint);
        reviewEntity.setUser(user);

        reviewEntity = reviewRepository.save(reviewEntity);
        return reviewEntity.toDto();
    }

    @Override
    public List<ReviewDto> getSprintReviewsByCategory(String sprintKey, String category) {
        List<ReviewEntity> reviews = reviewRepository.findAllBySprintKeyAndCategory(sprintKey, category);

        return entitiesToDtosConvertor.convertReviewEntitiesToDtos(reviews);
    }

    public void deleteReviews(String sprintKey) {
        List<ReviewEntity> reviewList = reviewRepository.findAllBySprintKey(sprintKey);
        for (ReviewEntity review : reviewList) {
            review.setUser(null);
            review.setSprint(null);
            review = this.reviewRepository.save(review);
            this.reviewRepository.delete(review.getId());
        }
    }
}
