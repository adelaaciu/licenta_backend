package service.impl;

import dto.SprintTaskDto;
import exception.backlog.BacklogNotFoundException;
import exception.sprint.SprintNotFoundException;
import exception.task.BacklogTaskNotFoundException;
import exception.task.TaskNotFoundException;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import repository.*;
import security.SecurityUtils;
import service.api.DtosToEntitiesConvertor;
import service.api.EntitiesToDtosConvertor;
import service.api.SprintTaskService;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Service
public class SprintTaskServiceImpl implements SprintTaskService {
    @Autowired
    private SprintTaskRepository sprintTaskRepository;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;
    @Autowired
    private SprintRepository sprintRepository;
    @Autowired
    private BacklogTaskRepository backlogTaskRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private BoardUserRepository boardUserRepository;
    @Autowired
    private DtosToEntitiesConvertor dtosToEntitiesConvertor;
    @Autowired
    private BacklogRepository backlogRepository;
    @Autowired
    private MailService mailService;
    @Autowired
    private UserRepository userRepository;

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public SprintTaskDto addTaskToSprint(String sprintKey, String taskKey) {
        BacklogTaskEntity backlogTaskEntity = backlogTaskRepository.findByKey(taskKey);
        BacklogEntity backlogEntity = backlogRepository.findByKey(backlogTaskEntity.getBacklog().getKey());

        if (backlogTaskEntity == null) {
            throw new BacklogNotFoundException();
        }

        SprintEntity sprintEntity = sprintRepository.findByKey(sprintKey);

        if (sprintEntity == null) {
            throw new SprintNotFoundException();
        }

        if (null == sprintEntity.getTasks()) {
            sprintEntity.setTasks(new ArrayList<>());
        }

        SprintTaskEntity sprintTaskEntity = new SprintTaskEntity();
        sprintTaskEntity.setSprint(sprintEntity);
        if (backlogTaskEntity.getAssignee() != null)
            sprintTaskEntity.setAssignee(backlogTaskEntity.getAssignee());
        if (backlogTaskEntity.getWatcher() != null)
            sprintTaskEntity.setWatcher(backlogTaskEntity.getWatcher());
        if (backlogTaskEntity.getActualizationDate() != null)
            sprintTaskEntity.setActualizationDate(backlogTaskEntity.getActualizationDate());
        sprintTaskEntity.setKey(taskKey);
        sprintTaskEntity.setTitle(backlogTaskEntity.getTitle());
        sprintTaskEntity.setDescription(backlogTaskEntity.getDescription());
        sprintTaskEntity.setStatus(backlogTaskEntity.getStatus());
        if (backlogTaskEntity.getStartDate() != null)
            sprintTaskEntity.setStartDate(backlogTaskEntity.getStartDate());
        if (backlogTaskEntity.getEndDate() != null)
            sprintTaskEntity.setEndDate(backlogTaskEntity.getEndDate());
        sprintTaskEntity.setStoryPoints(backlogTaskEntity.getStoryPoints());
        sprintTaskEntity.setPriority(backlogTaskEntity.getPriority());


        backlogTaskEntity.setComments(new ArrayList<>());
        backlogTaskEntity = backlogTaskRepository.save(backlogTaskEntity);
        backlogEntity.getTasks().remove(backlogTaskEntity);

        List<CommentEntity> comments = new ArrayList<>();
        List<CommentEntity> commentEntityList = commentRepository.findAllByTaskKey(taskKey);
        commentEntityList.forEach(x -> {

            CommentEntity comment = new CommentEntity();
            comment.setUser(x.getUser());
            comment.setDescription(x.getDescription());
            comments.add(comment);

            x.setUser(null);
            x.setTask(null);
            x = commentRepository.save(x);
            commentRepository.delete(x.getId());
        });

        backlogRepository.save(backlogEntity);
        backlogTaskRepository.delete(backlogTaskEntity);
        sprintTaskEntity = sprintTaskRepository.save(sprintTaskEntity);
        SprintTaskEntity finalSprintTaskEntity = sprintTaskEntity;

        comments.forEach(comment -> {
            comment.setTask(finalSprintTaskEntity);
            comment.setKey(taskKey + "-Comm-" + commentRepository.findAllByTaskKey(comment.getTask().getKey()).size() + 1);

            commentRepository.save(comment);
        });


        sprintTaskEntity.setComments(commentRepository.findAllByTaskKey(sprintTaskEntity.getKey()));
        sprintTaskEntity = sprintTaskRepository.save(sprintTaskEntity);
        sprintEntity.getTasks().add(sprintTaskEntity);
        sprintRepository.save(sprintEntity);

        SprintTaskDto taskDto = sprintTaskEntity.toDto();
        taskDto.setComments(entitiesToDtosConvertor.convertCommentEntityToDtoList(sprintTaskEntity.getComments()));

        return taskDto;
    }


    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<SprintTaskDto> getAllSprintTasks(String sprintKey) {
        List<SprintTaskEntity> list = sprintTaskRepository.findAllBySprintKey(sprintKey);
        if (null == list) {
            throw new BacklogTaskNotFoundException();
        }
        return entitiesToDtosConvertor.convertSprintTaskEntitiesToDtos(list);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public SprintTaskDto updateTask(SprintTaskDto task, String boardKey) {
        boolean sentMail = false;

        SprintTaskEntity taskEntity = sprintTaskRepository.findByKey(task.getKey());
        if (taskEntity == null) {
            throw new TaskNotFoundException();
        }

        task.setActualizationDate(new GregorianCalendar().getTime());

        if (taskEntity.getStatus().equals(Status.TODO.getMessage()) && task.getStatus().equals(Status.DEVIMPL.getMessage())) {
            task.setStartDate(new GregorianCalendar().getTime());
        }

        if (!taskEntity.getStatus().equals(Status.DONE.getMessage()) && task.getStatus().equals(Status.DONE.getMessage())) {
            task.setEndDate(new GregorianCalendar().getTime());
        }

        taskEntity = taskEntity.fromDto(task);
        SprintEntity sprint = sprintRepository.findByKey(task.getSprint().getKey());
        taskEntity.setSprint(sprint);
        if (null != task.getWatcher()) {
            taskEntity.setWatcher(boardUserRepository.findByUserUserMailAndBoardKey(task.getWatcher().getUser().getUser().getMail(), boardKey));
        }
        if (null != task.getAssignee()) {
            if (null == taskEntity.getAssignee() || !taskEntity.getAssignee().getUser().getUser().getMail().equals(task.getAssignee().getUser().getUser().getMail())) {
                UserEntity userEntity = userRepository.findByMail(task.getAssignee().getUser().getUser().getMail());
//                mailService.sendEmailTaskAssigned(userEntity, MailTemplate.ASSIGN_TASK, new Context(), task);
                sentMail = true;
            }

            taskEntity.setAssignee(boardUserRepository.findByUserUserMailAndBoardKey(task.getAssignee().getUser().getUser().getMail(), boardKey));
        }

        List<CommentEntity> comments = dtosToEntitiesConvertor.commentDtoListToEntityList(task.getComments(), sprint.getBoard().getKey());


        comments.forEach(comment -> {
            if (comment.getKey() == null) {
                comment.setKey(comment.getTask().getKey() + "-Comm-" + commentRepository.findAllByTaskKey(comment.getTask().getKey()).size() + 1);
                commentRepository.save(comment);
            }
        });

        taskEntity.setComments(commentRepository.findAllByTaskKey(task.getKey()));

        sprintTaskRepository.save(taskEntity);
        if (sentMail && task.getAssignee() != null) {
            UserEntity userEntity = userRepository.findByMail(task.getAssignee().getUser().getUser().getMail());
//            mailService.sendEmailTaskUpdated(userEntity, MailTemplate.ASSIGN_TASK, new Context(), task);
        }

        return task;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<SprintTaskDto> getByTitleContaining(String title) {
        List<SprintTaskEntity> list = sprintTaskRepository.findAllByTitleContainingIgnoringCase(title);
        if (null == list) {
            throw new TaskNotFoundException();
        }
        return entitiesToDtosConvertor.convertSprintTaskEntitiesToDtos(list);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<SprintTaskDto> getUserTasks() {
        String userMail = SecurityUtils.getCurrentUserLogin();
        List<SprintTaskEntity> list = sprintTaskRepository.findAllByAssigneeUserUserMail(userMail);
        if (null == list) {
            throw new TaskNotFoundException();
        }
        return entitiesToDtosConvertor.convertSprintTaskEntitiesToDtos(list);
    }


    private void deleteTaskFromSprint(String taskKey, SprintEntity sprintEntity) {
        SprintTaskEntity task = sprintTaskRepository.findByKey(taskKey);

        sprintEntity.getTasks().remove(task);
        sprintRepository.save(sprintEntity);

        task.setComments(new ArrayList<>());
        task.setAssignee(null);
        task.setWatcher(null);
        List<CommentEntity> commentEntities = commentRepository.findAllByTaskKey(taskKey);
        commentEntities.forEach(x -> {
            x.setUser(null);
            x.setTask(null);
            commentRepository.delete(x);
        });
        sprintTaskRepository.delete(task.getId());
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void removeAllTasksFromSprint(String sprintKey) {
        SprintEntity sprintEntity = sprintRepository.findByKey(sprintKey);

        List<SprintTaskEntity> taskList = sprintTaskRepository.findAllBySprintKey(sprintKey);

        for (SprintTaskEntity task : taskList) {
            this.deleteTaskFromSprint(task.getKey(), sprintEntity);
        }
    }

    @Override
    public List<SprintTaskDto> getDoneTasks(String sprintKey) {
        List<SprintTaskEntity> list = sprintTaskRepository.findAllBySprintKeyOrderByEndDate(sprintKey);
        List<SprintTaskEntity> endedTasks = new ArrayList<>();

        list.forEach(task -> {
            if (task.getEndDate() != null && task.getStatus().equalsIgnoreCase("done")) {
                endedTasks.add(task);
            }
        });

        return entitiesToDtosConvertor.convertSprintTaskEntitiesToDtos(endedTasks);
    }

}
