package service.impl;

import dto.BoardDto;
import dto.CommentDto;
import exception.task.TaskNotFoundException;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import repository.BoardRepository;
import repository.BoardUserRepository;
import repository.CommentRepository;
import repository.TaskRepository;
import service.api.*;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;
    @Autowired
    private BoardUserRepository boardUserRepository;
    @Autowired
    private BoardUserService boardUserService;
    @Autowired
    private BoardRepository boardRepository;

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public CommentDto addComment(CommentDto comment) {
        CommentEntity commentEntity = new CommentEntity().fromDto(comment);
        commentEntity.setUser(boardUserRepository.findByUserUserMail(comment.getUserEmail()));
        TaskEntity taskEntity = taskRepository.findByKey(comment.getTaskKey());

        commentEntity.setTask(taskEntity);

        if (null == taskEntity.getComments()) {
            taskEntity.setComments(new ArrayList<>());
        }
        commentEntity = commentRepository.save(commentEntity);

        taskEntity.getComments().add(commentEntity);
        taskRepository.save(taskEntity);

        return commentEntity.toDto();
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<CommentDto> getComments(String taskKey) {
        if (taskRepository.findByKey(taskKey) == null) {
            throw new TaskNotFoundException();
        }
        List<CommentEntity> commentEntities = commentRepository.findAllByTaskKey(taskKey);
        return entitiesToDtosConvertor.convertCommentEntityToDtoList(commentEntities);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void deleteUserComment(String userMail, BoardEntity boardEntity, ProjectEntity project) {
        List<CommentEntity> userComments = commentRepository.findAllByUserUserUserMail(userMail);
        for (CommentEntity comment : userComments) {

            BoardUserEntity anonymousBoardUser = boardUserService.createAnonymousBoardUser(boardEntity, project);
            comment.setUser(anonymousBoardUser);
            commentRepository.save(comment);
        }

    }
}
