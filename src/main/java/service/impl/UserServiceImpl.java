package service.impl;

import dto.UserDto;
import exception.user.UserAlreadyExistException;
import exception.user.UserNotFoundException;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import repository.ProjectRepository;
import repository.ProjectUserRepository;
import repository.UserRepository;
import security.SecurityUtils;
import service.api.*;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;
    @Autowired
    private ProjectUserRepository projectUserRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ProjectUserService projectUserService;

    /**
     * {@inheritDoc}
     */
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<UserDto> getAllUsers() {
        List<UserEntity> users = userRepository.findAll();
        if (users == null) {
            throw new UserNotFoundException();
        }

        final List<UserEntity> anonymous = new ArrayList<>();
        users.forEach(user -> {
            if (user.getMail().equalsIgnoreCase("anonymous@thinkagile.com")) {
                anonymous.add(user);
            }
        });

        anonymous.forEach(user -> {
            users.remove(user);
        });

        return entitiesToDtosConvertor.convertUserEntityToDtoList(users);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<UserDto> getUsersByName(String name) {
        List<UserEntity> users = userRepository.findAllByFirstNameContainingOrLastNameContainingIgnoringCase(name, name);
        if (users == null) {
            throw new UserNotFoundException();
        }
        if (users == null) {
            users.forEach(user -> {
                if (user.getMail().contains("anonymous")) {
                    users.remove(user);
                }
            });
        }

        return entitiesToDtosConvertor.convertUserEntityToDtoList(users);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserDto createUser(UserDto userDTO) {
        if (userRepository.findByMail(userDTO.getMail()) != null) {
            throw new UserAlreadyExistException();
        }
        UserEntity userEntity = new UserEntity().fromDto(userDTO);
        userEntity.setPassword(new PasswordGenerator().nextString());
        UserEntity user = userRepository.save(userEntity);
        return user.toDto();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public UserDto updateUser(UserDto userDTO) {
        UserEntity user = userRepository.findByMail(userDTO.getMail());

        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setRole(userDTO.getRole());
        user.setMail(userDTO.getMail());
        user.setLastName(userDTO.getLastName());
        user.setFirstName(userDTO.getFirstName());
        user.setPhoneNumber(userDTO.getPhoneNumber());
        user.setBirthday(userDTO.getBirthday());
        return userRepository.save(user).toDto();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteUserByEmail(String email) {
        UserEntity user = userRepository.findByMail(email);
        if (user == null) {
            throw new UserNotFoundException();
        }

        List<ProjectUserEntity> users = projectUserRepository.findByUserMailContaining(email);
        users.forEach(x -> {

            String projectName = x.getProject().getName();
            ProjectEntity project = projectRepository.findByName(projectName);

            projectUserService.deleteProjectUser(project, x);
            /*
            boardService.getProjectBoards(projectName).forEach( boardDto -> {
                    boardUserService.deleteBoardUser(x.getUser().getMail(), boardDto.getKey());
            });

            project.getUsers().remove(x);
            projectRepository.save(project);

            x.setUser(null);
            x.setProject(null);
            projectUserRepository.delete(x.getId());*/
        });


        userRepository.delete(user);
    }

    @Override
//    @PreAuthorize("hasAnyRole('role_user', 'ROLE_ADMIN')")
    public UserDto getCurrentUser() {
        try {
            String userMail = SecurityUtils.getCurrentUserLogin();
            UserEntity user = userRepository.findByMail(userMail);
            return user.toDto();
        } catch (UserNotFoundException e) {
            throw e;
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public boolean isCurrentUserInRole(String role) {
        return SecurityUtils.isCurrentUserInRole(role);
    }

}
