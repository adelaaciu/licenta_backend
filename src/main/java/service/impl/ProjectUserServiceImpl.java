package service.impl;

import dto.BoardDto;
import dto.ProjectUserDto;
import exception.project.ProjectNotFoundException;
import exception.user.ProjectUsersNotFoundException;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import repository.*;
import security.SecurityUtils;
import service.api.BoardService;
import service.api.BoardUserService;
import service.api.EntitiesToDtosConvertor;
import service.api.ProjectUserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectUserServiceImpl implements ProjectUserService {
    @Autowired
    private ProjectUserRepository projectUserRepository;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BoardUserRepository boardUserRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private BoardService boardService;
    @Autowired
    private BoardUserService boardUserService;
    @Autowired
    private MailService mailService;

    @Autowired
    private BoardRepository boardRepository;

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<ProjectUserDto> getProjectUsers(String projectName) {
        List<ProjectUserEntity> projectUserEntities = projectUserRepository.findAllByProjectName(projectName);
        if (null == projectUserEntities) {
            throw new ProjectUsersNotFoundException();
        }


        final List<ProjectUserEntity> anonymous = new ArrayList<>();
        projectUserEntities.forEach(projectUser -> {
            if (projectUser.getUser().getMail().equalsIgnoreCase("anonymous@thinkagile.com")) {
                anonymous.add(projectUser);
            }
        });

        anonymous.forEach(user -> {
            projectUserEntities.remove(user);
        });

        return entitiesToDtosConvertor.convertProjectUsersEntityToDtoList(projectUserEntities);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<ProjectUserDto> updateProjectUsers(String projectName, List<String> usersMail) {
        ProjectEntity projectEntity = projectRepository.findByName(projectName);
        if (projectEntity == null) {
            throw new ProjectNotFoundException();
        }

        List<UserEntity> users = userRepository.findAll();
        List<ProjectUserEntity> projectUsers = projectUserRepository.findAllByProjectName(projectName);

        projectUsers = removeUsersFromList(projectEntity, usersMail, projectUsers);

        if (usersMail != null || usersMail.size() > 0) {
            projectUsers = addUsersToList(usersMail, projectUsers, users, projectEntity);
        }


        projectEntity.setUsers(projectUsers);
        projectRepository.save(projectEntity);

        projectUsers.forEach(projectUserEntity -> {
//            mailService.sendEmailProjectAssigned(projectUserEntity.getUser(), MailTemplate.ASSIGN_PROJECT, new Context(), projectUserEntity.getProject().toDto());
        });

        return entitiesToDtosConvertor.convertProjectUsersEntityToDtoList(projectUsers);
    }

    public ProjectUserEntity createAnonymousProjectUser(ProjectEntity project) {
        ProjectUserEntity anonymousPrjUser = new ProjectUserEntity();
        anonymousPrjUser.setProject(project);
        anonymousPrjUser.setProjectRole(ProjectUserRole.USER.getProjectRole());
        anonymousPrjUser.setUser(userRepository.findByMail("anonymous@thinkagile.com"));
        anonymousPrjUser = projectUserRepository.save(anonymousPrjUser);
        return anonymousPrjUser;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public ProjectUserDto addProjectOwner(String projectName, String pmMail) {
        List<UserEntity> users = userRepository.findAll();
        ProjectEntity projectEntity = projectRepository.findByName(projectName);

        if (projectEntity == null) {
            throw new ProjectNotFoundException();
        }

        ProjectUserEntity projectUserEntity = new ProjectUserEntity();
        if (pmMail == null || pmMail.isEmpty()) {
            return null;
        } else {
            List<ProjectUserEntity> projectUsers = projectUserRepository.findAllByProjectName(projectName);

            if (!projectUsers.isEmpty()) {
                for (ProjectUserEntity user : projectUsers) {
                    if ( null != user.getProjectRole() && ((user.getProjectRole().equals(ProjectUserRole.PRODUCT_OWNER.getProjectRole()) && !pmMail.equals(user.getUser().getMail())) ||
                            (user.getProjectRole().equals(ProjectUserRole.USER.getProjectRole()) && pmMail.equals(user.getUser().getMail())))) {
                        deleteProjectUser(projectEntity, user);
                    }
                }
            }
            for (UserEntity user : users) {
                if (user.getMail().equals(pmMail)) {
                    projectUserEntity.setUser(user);
                    projectUserEntity.setProjectRole(ProjectUserRole.PRODUCT_OWNER.getProjectRole());
                    projectUserEntity.setProject(projectEntity);
                    projectUserEntity = projectUserRepository.save(projectUserEntity);
                    projectEntity.setUsers(projectUserRepository.findAllByProjectName(projectName));
                    projectEntity = projectRepository.save(projectEntity);

                }
            }

        }

        for (BoardEntity board : boardRepository.findAllByProjectName(projectName)) {
            BoardUserEntity boardUserEntity = new BoardUserEntity();
            boardUserEntity.setBoard(board);
            boardUserEntity.setUser(projectUserEntity);
            boardUserRepository.save(boardUserEntity);
            if(board.getBoardUsers() == null) {
                board.setBoardUsers(new ArrayList<>());
            }
            board.getBoardUsers().add(boardUserEntity);
            boardRepository.save(board);

            boardUserRepository.findByUserUserMailAndBoardKey(projectUserEntity.getUser().getMail(), board.getKey());

        }

        projectEntity.setBoards(boardRepository.findAllByProjectName(projectName));
        projectEntity.setUsers(projectUserRepository.findAllByProjectName(projectName));
        projectRepository.save(projectEntity);

        if (projectUserEntity.getProjectRole() != "") {
            return projectUserEntity.toDto();
        }

        return null;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void deleteProjectUserByParams(String projectName, String userMail) {
        ProjectEntity project = projectRepository.findByName(projectName);
        ProjectUserEntity user = projectUserRepository.findByUserMailAndProjectName(userMail, projectName);

        deleteProjectUser(project, user);
    }


    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<ProjectUserDto> getAllByUserMail(String mail) {
        List<ProjectUserEntity> projectUserEntities = projectUserRepository.findByUserMailContaining(mail);
        if (null == projectUserEntities) {
            throw new ProjectUsersNotFoundException();
        }

        final List<ProjectUserEntity> anonymous = new ArrayList<>();
        projectUserEntities.forEach(user -> {
            if (user.getUser().getMail().equalsIgnoreCase("anonymous@thinkagile.com")) {
                anonymous.add(user);
            }
        });

        anonymous.forEach(user -> {
            projectUserEntities.remove(user);
        });
        return entitiesToDtosConvertor.convertProjectUsersEntityToDtoList(projectUserEntities);

    }

    @Override
    public void deleteProjectUsers(String projectName) {
        ProjectEntity project = projectRepository.findByName(projectName);
        project.setUsers(new ArrayList<>());
        projectRepository.save(project);

        List<ProjectUserEntity> projectUserEntities = projectUserRepository.findAllByProjectName(projectName);
        projectUserEntities.forEach(x -> {
            deleteProjectUser(project, x);
        });
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void deleteProjectUser(ProjectEntity project, ProjectUserEntity projectUser) {
        List<BoardDto> projectBoards = boardService.getProjectBoards(project.getName());
        if(null != projectBoards) {
            projectBoards.forEach(boardDto -> {
                boardUserService.deleteBoardUser(projectUser.getUser().getMail(), boardDto.getKey());
            });
        }

        project.getUsers().remove(projectUser);
        projectRepository.save(project);

        projectUser.setUser(null);
        projectUser.setProject(null);
        projectUserRepository.delete(projectUser.getId());
    }


    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public ProjectUserDto getProjectUserByUserMailAndProjectName(String projectName) {
        String userMail = SecurityUtils.getCurrentUserLogin();
        ProjectUserEntity projectUserEntity = projectUserRepository.findByUserMailAndProjectName(userMail, projectName);
        if (null == projectUserEntity) {
            throw new ProjectUsersNotFoundException();
        }

        return projectUserEntity.toDto();
    }


    private List<ProjectUserEntity> removeUsersFromList(ProjectEntity projectEntity, List<String> usersMail, List<ProjectUserEntity> users) {
        List<ProjectUserEntity> finalUsers = new ArrayList<>();
        if (null != users && users.size() > 0) {
            users.forEach(u -> {
                if (null != usersMail && (usersMail.size() == 0 || !usersMail.contains(u.getUser().getMail()) && !u.getProjectRole().equals(ProjectUserRole.PRODUCT_OWNER.getProjectRole()))) {
                    finalUsers.add(u);
                }
            });
        }

        ProjectUserEntity anonymous = createAnonymousProjectUser(projectEntity);
        finalUsers.forEach(usr -> {
            users.remove(usr);
            projectEntity.getUsers().remove(usr);
            projectRepository.save(projectEntity);

            List<BoardUserEntity> boardUserEntities = boardUserRepository.findAllByUserProjectName(projectEntity.getName());
            boardUserEntities.forEach(boardUser -> {
                boardUser.setUser(anonymous);
                boardUserRepository.save(boardUser);
            });


            projectUserRepository.delete(usr);
        });

        return users;
    }

    private List<ProjectUserEntity> addUsersToList(List<String> usersMail, List<ProjectUserEntity> projectUsers, List<UserEntity> users, ProjectEntity projectEntity) {

        users.forEach(projectUserEntity -> {
            String mail = projectUserEntity.getMail();
            if (usersMail.contains(mail)) {
                ProjectUserEntity projectUser = projectUserRepository.findByUserMailAndProjectName(mail, projectEntity.getName());
                if (projectUser == null) {
                    projectUser = new ProjectUserEntity();
                    projectUser.setUser(projectUserEntity);
                    projectUser.setProjectRole(ProjectUserRole.USER.getProjectRole());
                    projectUser.setProject(projectEntity);
                    projectUsers.add(projectUser);
                } else {
                    if (projectUser.getProjectRole() != ProjectUserRole.USER.getProjectRole()) {
                        projectUser.setProjectRole(ProjectUserRole.USER.getProjectRole());
                        projectUserRepository.save(projectUser);
                    }
                }
            }
        });

        projectUsers.forEach(user -> {
            projectUserRepository.save(user);
        });

        return projectUsers;
    }
}
