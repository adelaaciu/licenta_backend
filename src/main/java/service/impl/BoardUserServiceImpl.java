package service.impl;

import dto.BoardUserDto;
import exception.board.BoardNotFoundException;
import exception.task.TaskNotFoundException;
import exception.user.BoardUsersNotFoundException;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import repository.*;
import security.SecurityUtils;
import service.api.BoardUserService;
import service.api.CommentService;
import service.api.EntitiesToDtosConvertor;
import service.api.ProjectUserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class    BoardUserServiceImpl implements BoardUserService {
    @Autowired
    private BoardUserRepository boardUserRepository;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;
    @Autowired
    private BoardRepository boardRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ProjectUserRepository projectUserRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommentService commentService;
    @Autowired
    private ProjectUserService projectUserService;
    @Autowired
    private MailService mailService;

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<BoardUserDto> updateBoardUsers(String boardKey, List<String> usersMail) {
        BoardEntity boardEntity = boardRepository.findByKey(boardKey);
        if (boardEntity == null) {
            throw new BoardNotFoundException();
        }

        ProjectEntity project = projectRepository.findByName(boardEntity.getProject().getName());
        List<ProjectUserEntity> users = projectUserRepository.findAllByProjectName(project.getName());
        List<BoardUserEntity> boardEntities = boardUserRepository.findAllByBoardKey(boardKey);

        BoardUserEntity anonymousBoardUser = createAnonymousBoardUser(boardEntity, project);

        boardEntities = removeUsersFromList(boardEntity, usersMail, boardEntities, anonymousBoardUser);
        if (usersMail != null || usersMail.size() > 0) {
            boardEntities = addUsersToList(usersMail, boardEntities, users, boardEntity);
        }

        boardEntity.setBoardUsers(boardEntities);
        boardRepository.save(boardEntity);


        boardEntities.forEach(userEntity -> {
//            mailService.sendEmailBoardAssigned(userEntity.getUser().getUser(), MailTemplate.ASSIGN_BOARD, new Context(), userEntity.getBoard().toDto());
        });


        return entitiesToDtosConvertor.convertBoardUserEntitiesToDtos(boardEntities);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public BoardUserEntity createAnonymousBoardUser(BoardEntity boardEntity, ProjectEntity project) {
//        ProjectUserEntity anonymousPrjUser = new ProjectUserEntity();
//        anonymousPrjUser.setProject(project);
//        anonymousPrjUser.setProjectRole(ProjectUserRole.USER.getProjectRole());
//        anonymousPrjUser.setUser(userRepository.findByMail("anonymous@thinkagile.com"));
//        anonymousPrjUser = projectUserRepository.save(anonymousPrjUser);

        ProjectUserEntity anonymousProjectUser = projectUserService.createAnonymousProjectUser(project);

        BoardUserEntity anonymousBoardUserEntity = new BoardUserEntity();
        anonymousBoardUserEntity.setUser(anonymousProjectUser);
        anonymousBoardUserEntity.setBoard(boardEntity);
        anonymousBoardUserEntity = boardUserRepository.save(anonymousBoardUserEntity);
        return anonymousBoardUserEntity;
    }

    @Override
    public BoardUserDto getCurrentBoardUserPM(String boardKey) {
        BoardUserDto currentBoardUser = getCurrentBoardUser(boardKey);
        if(currentBoardUser.getUser().getProjectRole().equals(ProjectUserRole.PRODUCT_OWNER.getProjectRole())){
            return currentBoardUser;
        }
        return null;
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    private List<BoardUserEntity> removeUsersFromList(BoardEntity board, List<String> usersMail, List<BoardUserEntity> users, BoardUserEntity anonymousBoardUser) {
        List<BoardUserEntity> finalUsers = new ArrayList<>();
        if (null != users && users.size() > 0) {
            users.forEach(u -> {
                if (usersMail.size() == 0 || !usersMail.contains(u.getUser().getUser().getMail())) {
                    finalUsers.add(u);
                }
            });
        }

        finalUsers.forEach(usr -> {
            users.remove(usr);
            board.getBoardUsers().remove(usr);
            boardRepository.save(board);

            List<CommentEntity> comments = commentRepository.findAllByUser(usr);
            comments.forEach(comment -> {
                comment.setUser(anonymousBoardUser);
            });

            boardUserRepository.delete(usr.getId());
        });

        return users;
    }

    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    private List<BoardUserEntity> addUsersToList(List<String> usersMail, List<BoardUserEntity> users, List<ProjectUserEntity> projectUsers, BoardEntity boardEntity) {

        projectUsers.forEach(projectUserEntity -> {
            String mail = projectUserEntity.getUser().getMail();
            if (usersMail.contains(mail)) {
                BoardUserEntity boardUserEntity = boardUserRepository.findByUserUserMailAndBoardKey(mail, boardEntity.getKey());
                if (boardUserEntity == null) {
                    boardUserEntity = new BoardUserEntity();
                    boardUserEntity.setUser(projectUserEntity);
                    boardUserEntity.setBoard(boardEntity);
                    boardUserEntity = boardUserRepository.save(boardUserEntity);
                    users.add(boardUserEntity);
                }
            }
        });

        return users;
    }


    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<BoardUserDto> getAllBoardUsers(String boardKey) {
        BoardEntity boardEntity = boardRepository.findByKey(boardKey);
        if (boardEntity == null) {
            throw new BoardNotFoundException();
        }

        List<BoardUserEntity> users = boardUserRepository.findAllByBoardKey(boardKey);

        final List<BoardUserEntity> anonymous = new ArrayList<>();
        users.forEach(user -> {
            if (user.getUser().getUser().getMail().equalsIgnoreCase("anonymous@thinkagile.com")) {
                anonymous.add(user);
            }
        });

        anonymous.forEach(user -> {
            users.remove(user);
        });

        return entitiesToDtosConvertor.convertBoardUserEntitiesToDtos(users);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public BoardUserDto addWatcherToTask(String taskKey, BoardUserDto dto) {
        TaskEntity taskEntity = taskRepository.findByKey(taskKey);
        if (null == taskEntity) {
            throw new TaskNotFoundException();
        }

        BoardUserEntity boardUserEntity = new BoardUserEntity().fromDto(dto);
        taskEntity.setWatcher(boardUserEntity);
        taskRepository.save(taskEntity);

        return boardUserEntity.toDto();
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public BoardUserDto addAssigneeToTask(String taskKey, BoardUserDto dto) {
        TaskEntity taskEntity = taskRepository.findByKey(taskKey);
        if (null == taskEntity) {
            throw new TaskNotFoundException();
        }

        BoardUserEntity boardUserEntity = new BoardUserEntity().fromDto(dto);
        taskEntity.setAssignee(boardUserEntity);
        taskRepository.save(taskEntity);

        return boardUserEntity.toDto();
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void deleteBoardUsers(String boardKey) {
        List<BoardUserEntity> users = boardUserRepository.findAllByBoardKey(boardKey);
        users.forEach(user -> {
            deleteBoardUser(user.getUser().getUser().getMail(), boardKey);
        });

    }

    @Override
//    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public BoardUserDto getCurrentBoardUser(String boardKey) {
        String userMail = SecurityUtils.getCurrentUserLogin();
        boardRepository.findByKey(boardKey);
        BoardUserEntity boardUserEntity = boardUserRepository.findByUserUserMailAndBoardKey(userMail, boardKey);
        if (boardUserEntity == null) {
            throw new BoardUsersNotFoundException();
        }
        return boardUserEntity.toDto();
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void deleteBoardUser(String userMail, String boardKey) {
        BoardUserEntity boardUserEntity = boardUserRepository.findByUserUserMailAndBoardKey(userMail, boardKey);
        if (boardUserEntity != null) {

            BoardEntity board = boardRepository.findByKey(boardKey);
            commentService.deleteUserComment(userMail, board, board.getProject());
            board.getBoardUsers().remove(boardUserEntity);

            List<TaskEntity> tasks = taskRepository.findByAssigneeUserUserMail(userMail);
            if (tasks != null) {
                tasks.forEach(task -> {
                    task.setAssignee(null);
                    taskRepository.save(task);
                });
            }
            tasks = taskRepository.findByWatcherUserUserMail(userMail);
            if (tasks != null) {
                tasks.forEach(task -> {
                    task.setWatcher(null);
                    taskRepository.save(task);
                });
            }

            boardUserEntity.setUser(null);
            boardUserEntity.setBoard(null);
            boardUserRepository.delete(boardUserEntity.getId());
        }
    }
}
