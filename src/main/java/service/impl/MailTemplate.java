package service.impl;

public enum MailTemplate {
    ASSIGN_TASK("assign_task", "You have a new assigned task"),
    ASSIGN_PROJECT("assigned_project", "You have a new assigned project"),
    ASSIGN_BOARD("assigned_board", "You have a new assigned board"),
    UPDATE_TASK("assign_task", "Your task was updated");


    private String type;
    private String subjectKey;

    MailTemplate(String type, String subjectKey) {
        this.type = type;
        this.subjectKey = subjectKey;
    }

    public String toString() {
        return this.type;
    }

    public String getSubjectKey() {
        return this.subjectKey;
    }

}

