package service.impl;

import dto.*;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.BoardUserRepository;
import repository.CommentRepository;
import repository.SprintRepository;
import repository.SprintTaskRepository;
import service.api.BoardService;
import service.api.EntitiesToDtosConvertor;
import service.api.ProjectUserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class EntitiesToDtosConvertorImpl implements EntitiesToDtosConvertor {
    @Autowired
    private ProjectUserService projectUserService;
    @Autowired
    private BoardService boardService;
    @Autowired
    private BoardUserRepository boardUserRepository;
    @Autowired
    private SprintRepository sprintRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private SprintTaskRepository sprintTaskRepository;

    @Override
    public List<ProjectUserDto> convertProjectUsersEntityToDtoList(List<ProjectUserEntity> users) {
        List<ProjectUserDto> dtos = new ArrayList<>();

        if (users != null || !users.isEmpty()) {
            users.forEach(user -> {
                dtos.add(user.toDto());
            });
        }

        return dtos;
    }

    @Override
    public List<BoardDto> convertBoardEntitiesToDtos(List<BoardEntity> boardEntities) {
        List<BoardDto> dtos = new ArrayList<>();

        if (boardEntities != null || !boardEntities.isEmpty()) {
            boardEntities.forEach(boardEntity -> {
                BoardDto boardDto = boardEntity.toDto();
                boardDto.setBoardUsers(convertBoardUserEntitiesToDtos(boardUserRepository.findAllByBoardKey(boardEntity.getKey())));
                boardDto.setSprints(convertSprintEntitiesToDtos(sprintRepository.findAllByBoardKey(boardEntity.getKey())));
                dtos.add(boardDto);
            });
        }

        return dtos;
    }

    @Override
    public List<SprintDto> convertSprintEntitiesToDtos(List<SprintEntity> sprints) {

        List<SprintDto> dtos = new ArrayList<>();
        if (sprints != null || !sprints.isEmpty()) {
            sprints.forEach(sprintEntity -> {
                SprintDto sprintDto = sprintEntity.toDto();
                sprintDto.setTasks(convertSprintTaskEntitiesToDtos(sprintTaskRepository.findAllBySprintKey(sprintEntity.getKey())));
                dtos.add(sprintDto);
            });
        }

        return dtos;
    }

    public List<SprintTaskDto> convertSprintTaskEntitiesToDtos(List<SprintTaskEntity> sprintTaskEntities) {
        List<SprintTaskDto> dtos = new ArrayList<>();
        if (sprintTaskEntities != null || !sprintTaskEntities.isEmpty()) {
            sprintTaskEntities.forEach(sprintTaskEntity -> {
                SprintTaskDto dto = (SprintTaskDto) sprintTaskEntity.toDto();
                dto.setComments(convertCommentEntityToDtoList(commentRepository.findAllByTaskKey(sprintTaskEntity.getKey())));
                dtos.add(dto);
            });
        }

        return dtos;
    }

    @Override
    public List<TaskDto> convertTaskEntitiesToDtos(List<TaskEntity> tasks) {
        List<TaskDto> dtos = new ArrayList<>();
        if (tasks != null || !tasks.isEmpty()) {
            tasks.forEach(task -> {
                TaskDto dto = task.toDto();
                dto.setComments(convertCommentEntityToDtoList(commentRepository.findAllByTaskKey(task.getKey())));
                dtos.add(dto);
            });
        }

        return dtos;
    }

    public List<BoardUserDto> convertBoardUserEntitiesToDtos(List<BoardUserEntity> users) {
        List<BoardUserDto> dtos = new ArrayList<>();
        if (users != null || !users.isEmpty()) {
            users.forEach(user -> {
                BoardUserDto boardUserDto = user.toDto();
                dtos.add(boardUserDto);
            });
        }

        return dtos;
    }


    @Override
    public List<ProjectDto> convertProjectEntityToDtoList(List<ProjectEntity> list) {
        List<ProjectDto> dtos = new ArrayList<>();
        if (list != null || !list.isEmpty()) {
            list.forEach(project -> {
                ProjectDto projectDto = project.toDto();
                projectDto.setUsers(projectUserService.getProjectUsers(projectDto.getName()));
                projectDto.setBoards(boardService.getProjectBoards(projectDto.getName()));
                dtos.add(projectDto);
            });
        }

        return dtos;
    }

    @Override
    public List<CommentDto> convertCommentEntityToDtoList(List<CommentEntity> comments) {
        List<CommentDto> dtos = new ArrayList<>();
        if (comments != null || !comments.isEmpty()) {
            comments.forEach(x -> {
                CommentDto commentDto = x.toDto();
                dtos.add(commentDto);
            });
        }

        return dtos;
    }

    @Override
    public List<UserDto> convertUserEntityToDtoList(List<UserEntity> list) {
        List<UserDto> dtos = new ArrayList<>();
        if (list != null || !list.isEmpty()) {
            list.forEach(user -> {
                dtos.add(user.toDto());
            });
        }

        return dtos;
    }

    @Override
    public List<BacklogTaskDto> convertBacklogTaskEntitiesToDtod(List<BacklogTaskEntity> tasks) {
        List<BacklogTaskDto> dtos = new ArrayList<>();
        if (tasks != null || !tasks.isEmpty()) {
            tasks.forEach(x -> {
                BacklogTaskDto taskDto = x.toDto();
                taskDto.setComments(convertCommentEntityToDtoList(x.getComments()));
                dtos.add(taskDto);
            });
        }

        return dtos;
    }

    @Override
    public List<ReviewDto> convertReviewEntitiesToDtos(List<ReviewEntity> reviews) {
        List<ReviewDto> reviewDtos = new ArrayList<>();

        reviews.forEach(review -> {
            reviewDtos.add(review.toDto());
        });
        return reviewDtos;
    }
}
