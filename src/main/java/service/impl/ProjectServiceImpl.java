package service.impl;

import dto.ProjectDto;
import dto.ProjectUserDto;
import exception.project.ProjectAlreadyExistException;
import exception.project.ProjectNotFoundException;
import model.ProjectEntity;
import model.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import repository.ProjectRepository;
import security.SecurityUtils;
import service.api.BoardService;
import service.api.EntitiesToDtosConvertor;
import service.api.ProjectService;
import service.api.ProjectUserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private EntitiesToDtosConvertor entitiesToDtosConvertor;
    @Autowired
    private ProjectUserService projectUserService;
    @Autowired
    private BoardService boardService;

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<ProjectDto> getAllProjects() {
        List<ProjectEntity> projects = projectRepository.findAll();
        if (null == projects) {
            throw new ProjectNotFoundException();
        }
        return entitiesToDtosConvertor.convertProjectEntityToDtoList(projects);

    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<ProjectDto> getProjectsByNameContaining(String name) {
        List<ProjectEntity> projects = projectRepository.findAllByNameContainingIgnoringCase(name);
        if (null == projects || projects.isEmpty()) {
            throw new ProjectNotFoundException();
        }
        return entitiesToDtosConvertor.convertProjectEntityToDtoList(projects);
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ProjectDto createProject(ProjectDto projectDTO) {
//        System.out.println("************************** Create project");
        if (projectRepository.findByName(projectDTO.getName()) != null) {
            throw new ProjectAlreadyExistException();
        }

        ProjectEntity project = projectRepository.save(new ProjectEntity().fromDto(projectDTO));
        project.setBoards(new ArrayList<>());
        project.setUsers(new ArrayList<>());

        return project.toDto();
    }


    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteProject(String name) {
        ProjectEntity project = projectRepository.findByName(name);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        boardService.deleteProjectBoard(name);
        projectUserService.deleteProjectUsers(name);
        projectRepository.delete(project);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public List<ProjectDto> getProjectsByUser() {
        String mail = SecurityUtils.getCurrentUserLogin();
        List<ProjectUserDto> projectUsers = projectUserService.getAllByUserMail(mail);
        List<ProjectEntity> projects = new ArrayList<>();
        projectUsers.forEach(projectUserDto -> {
            final String projectName = projectUserDto.getProject().getName();
            final ProjectEntity projectEntity = projectRepository.findByName(projectName);
            projects.add(projectEntity);
        });

        return entitiesToDtosConvertor.convertProjectEntityToDtoList(projects);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public ProjectDto getProjectsByName(String name) {
        ProjectEntity projectEntity = projectRepository.findByName(name);
        if (null == projectEntity) {
            throw new ProjectNotFoundException();
        }

        ProjectDto projectDto = projectEntity.toDto();
        projectDto.setBoards(boardService.getProjectBoards(name));
        projectDto.setUsers(projectUserService.getProjectUsers(name));

        return projectDto;
    }

}
