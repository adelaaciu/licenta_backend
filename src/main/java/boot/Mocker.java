/*
package boot;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import repository.*;
import service.api.BoardService;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

@Component
public class Mocker {

    final BacklogRepository backlogRepository;
    final BacklogTaskRepository backlogTaskRepository;
    final BoardRepository boardRepository;
    final BoardUserRepository boardUserRepository;
    final CommentRepository commentRepository;
    final ProjectRepository projectRepository;
    final ProjectUserRepository projectUserRepository;
    final SprintRepository sprintRepository;
    final SprintTaskRepository sprintTaskRepository;
    final TaskRepository taskRepository;
    final UserRepository userRepository;
    final BoardService boardService;

    //
    @Autowired
    public Mocker(BacklogRepository backlogRepository, BacklogTaskRepository backlogTaskRepository, BoardRepository boardRepository, BoardUserRepository boardUserRepository, CommentRepository commentRepository, ProjectRepository projectRepository, ProjectUserRepository projectUserRepository, SprintRepository sprintRepository, SprintTaskRepository sprintTaskRepository, TaskRepository taskRepository, UserRepository userRepository, BoardService boardService) {
        this.backlogRepository = backlogRepository;
        this.backlogTaskRepository = backlogTaskRepository;
        this.boardRepository = boardRepository;
        this.boardUserRepository = boardUserRepository;
        this.commentRepository = commentRepository;
        this.projectRepository = projectRepository;
        this.projectUserRepository = projectUserRepository;
        this.sprintRepository = sprintRepository;
        this.sprintTaskRepository = sprintTaskRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
        this.boardService = boardService;
    }

    //    //
////thinkagileadm - created
//    //thinkagilepo
//    //thinkagileusr
////ThinkAgile1!
    @PostConstruct
    public void init() {
      */
/*  UserEntity anonymous = new UserEntity();
        anonymous.setMail("anonymous@thinkagile.com");
        anonymous.setFirstName("Anonymous");
        anonymous.setLastName("Anonymous");
        anonymous.setPassword("Anonymous");
        anonymous.setRole("Anonymous");
        anonymous = userRepository.save(anonymous);*//*


        UserEntity userEntity1 = new UserEntity();
        userEntity1.setRole(UserRole.ADMIN.getRole());
        userEntity1.setMail("admin");
        userEntity1.setPassword(new BCryptPasswordEncoder().encode("admin"));
        userEntity1.setFirstName("Marian");
        userEntity1.setLastName("Santauan");
        userEntity1.setPhoneNumber("0774856129");
        userEntity1.setBirthday("1990-01-15");
        userRepository.save(userEntity1);

        UserEntity userEntity2 = new UserEntity();
        userEntity2.setRole(UserRole.USER.getRole());
        userEntity2.setMail("adela.aciu@thinkagile.com");
        userEntity2.setPassword(new BCryptPasswordEncoder().encode("test"));
        userEntity2.setFirstName("Adela");
        userEntity2.setLastName("Aciu");
        userEntity2.setPhoneNumber("0774856349");
        userEntity2.setBirthday("1995-04-05");
        userEntity2 = userRepository.save(userEntity2);
//
        UserEntity userEntity3 = new UserEntity();
        userEntity3.setRole(UserRole.USER.getRole());
        userEntity3.setMail("ionel.popescu@thinkagile.com");
        userEntity3.setPassword(new BCryptPasswordEncoder().encode("abcd"));
        userEntity3.setFirstName("Ionel");
        userEntity3.setLastName("Popescu");
        userEntity3.setBirthday("1985-09-09");
        userEntity3.setPhoneNumber("0734856129");
        userEntity3 = userRepository.save(userEntity3);
*/
/*//*
/
//        UserEntity userEntity4 = new UserEntity();
//        userEntity4.setRole(UserRole.USER.getRole());
//        userEntity4.setMail("george.popovici@thinkagile.com");
//        userEntity4.setPassword("randomGen123*!");
//        userEntity4.setFirstName("George");
//        userEntity4.setLastName("Popovici");
//        userEntity4.setPhoneNumber("0724853129");
//        userEntity4 = userRepository.save(userEntity4);
//
//        UserEntity userEntity5 = new UserEntity();
//        userEntity5.setRole(UserRole.USER.getRole());
//        userEntity5.setMail("florin.marcus@thinkagile.com");
//        userEntity5.setPassword("randomGen123*!");
//        userEntity5.setFirstName("Florin");
//        userEntity5.setLastName("Marcus");
//        userEntity5.setPhoneNumber("0754856129");
//        userEntity5 = userRepository.save(userEntity5);
//
        ProjectEntity projectEntity1 = new ProjectEntity();
        projectEntity1.setName("WBC");
        projectEntity1.setDescription("Proiectul consta in realizarea unei aplicatii desktop in cadrul companiei X, care sa fie folosita de catre secretare. Detaliile de implementare vor putea fii gasite pe linkul: http://thinkagle.wbc.implementarion.details.com");
        projectEntity1 = projectRepository.save(projectEntity1);
//
//
        ProjectEntity projectEntity2 = new ProjectEntity();
        projectEntity2.setName("Do it");
        projectEntity2.setDescription("Aplicatie pentru sportivii de performanta pentru monitorizarea starii lor de sanatate");
        projectEntity2 = projectRepository.save(projectEntity2);
//
        ProjectUserEntity projectUserEntity1 = new ProjectUserEntity();
        projectUserEntity1.setUser(userEntity2);
        projectUserEntity1.setProject(projectEntity1);
        projectUserEntity1.setProjectRole(ProjectUserRole.PRODUCT_OWNER.getProjectRole());
        projectUserEntity1 = projectUserRepository.save(projectUserEntity1);

//
//
        List<ProjectUserEntity> projectUserEntities = new ArrayList<>();
        projectUserEntities.add(projectUserEntity1);
//        projectUserEntities.add(projectUserEntity2).;

        projectEntity1.setUsers(projectUserEntities);
//
//
        BoardEntity boardEntity1 = new BoardEntity();
        boardEntity1.setKey(projectEntity1.getName() + "-B-V1");
        boardEntity1.setName("DB impl");
        boardEntity1.setProject(projectEntity1);
        boardEntity1.setSprints(new ArrayList<>());
        boardEntity1.setBoardUsers(new ArrayList<>());
        boardEntity1 = boardRepository.save(boardEntity1);
//
        BoardEntity boardEntity2 = new BoardEntity();
        boardEntity2.setKey(projectEntity1.getName() + "-B-V2");
        boardEntity2.setName("Backlog-V2 - UI design");
        boardEntity2.setProject(projectEntity1);
        boardEntity2.setSprints(new ArrayList<>());
        boardEntity2.setBoardUsers(new ArrayList<>());
        boardEntity2 = boardRepository.save(boardEntity2);



//        BoardEntity boardEntity3 = new BoardEntity();
//        boardEntity3.setKey(projectEntity1.getName() + "-B-V13");
//        boardEntity3.setName("Test");
//        boardEntity3.setProject(projectEntity2);
//        boardEntity3.setSprints(new ArrayList<>());
//        boardEntity3.setBoardUsers(new ArrayList<>());
//        boardEntity3 = boardRepository.save(boardEntity3);

        BacklogEntity backlogEntity = new BacklogEntity();
        backlogEntity.setKey(boardEntity1.getKey() + "Bk-1");
        backlogEntity.setTasks(new ArrayList<>());
        backlogEntity = backlogRepository.save(backlogEntity);

        boardEntity1.setBacklog(backlogEntity);
        List<BacklogTaskEntity> backlogTaskEntities = new ArrayList<>();
//
        BacklogTaskEntity taskEntity1 = new BacklogTaskEntity();
        taskEntity1.setBacklog(backlogEntity);
        taskEntity1.setKey(backlogEntity.getKey() + "-T-1");
        taskEntity1.setTitle("VT-impl");
        taskEntity1.setDescription("DB schema");
        taskEntity1.setStatus(Status.TODO.getMessage());
        taskEntity1.setStoryPoints(1);
        taskEntity1.setPriority(Priority.LOW.getMessage());
        taskEntity1.setComments(new ArrayList<>());
        taskEntity1 = backlogTaskRepository.save(taskEntity1);
        backlogTaskEntities.add(taskEntity1);


        BacklogTaskEntity taskEntity2 = new BacklogTaskEntity();
        taskEntity2.setBacklog(backlogEntity);
        taskEntity2.setKey(backlogEntity.getKey() + "-T-2");
        taskEntity2.setTitle("VT-impl2");
        taskEntity2.setDescription("DB table...");
        taskEntity2.setStatus(Status.TODO.getMessage());
        taskEntity2.setStoryPoints(2);
        taskEntity2.setPriority(Priority.LOW.getMessage());
        taskEntity2.setComments(new ArrayList<>());
        taskEntity2 = backlogTaskRepository.save(taskEntity2);
        backlogTaskEntities.add(taskEntity2);
//
        boardEntity1.setBacklog(backlogEntity);
        boardRepository.save(boardEntity1);

        BoardUserEntity boardUserEntity = new BoardUserEntity();
        boardUserEntity.setBoard(boardEntity1);
        boardUserEntity.setUser(projectUserEntity1);
        boardUserRepository.save(boardUserEntity);
        boardEntity1.getBoardUsers().add(boardUserEntity);
        boardRepository.save(boardEntity1);


        BoardUserEntity boardUserEntity2 = new BoardUserEntity();
        boardUserEntity2.setBoard(boardEntity2);
        boardUserEntity2.setUser(projectUserEntity1);
        boardUserRepository.save(boardUserEntity2);
        boardEntity2.getBoardUsers().add(boardUserEntity2);
        boardRepository.save(boardEntity2);

        CommentEntity commentEntity = new CommentEntity();
        commentEntity.setTask(taskEntity1);
        commentEntity.setUser(boardUserEntity);
        commentEntity.setDescription("Must be reanalisedf");
        commentEntity.setKey(taskEntity1.getKey() + "Comm1");
        commentRepository.save(commentEntity);
//
        taskEntity1.getComments().add(commentEntity);
        taskRepository.save(taskEntity1);

        List<BoardEntity> boardEntities = new ArrayList<>();
        boardEntities.add(boardEntity1);
//        boardEntities.add(boardEntity2);
        projectEntity1.setBoards(boardEntities);


        SprintEntity sprintEntity = new SprintEntity();
        sprintEntity.setBoard(boardEntity1);
        List<SprintEntity> sprintEntities = sprintRepository.findAllByBoardKey(boardEntity1.getKey());
        sprintEntity.setSprintNo(sprintEntities.size() + 1);
        sprintEntity.setStartDate(new GregorianCalendar(2018, 5, 2).getTime());
        sprintEntity.setClosed(false);
        sprintEntity.setTasks(new ArrayList<>());
        sprintEntity.setKey(boardEntity1.getKey() + "-S-" + sprintEntity.getSprintNo());
        sprintEntity.setDays(5);
        sprintEntity = sprintRepository.save(sprintEntity);
        boardEntity1.setSprints(sprintRepository.findAllByBoardKey(boardEntity1.getKey()));

        SprintTaskEntity sprintTaskEntity = new SprintTaskEntity();
        sprintTaskEntity.setSprint(sprintEntity);
        sprintTaskEntity.setKey(backlogEntity.getKey() + "-T-41");
        sprintTaskEntity.setTitle("Dummy title");
        sprintTaskEntity.setDescription("sa asd");
        sprintTaskEntity.setEndDate(new GregorianCalendar(2018, 5, 11).getTime());
        sprintTaskEntity.setStatus(Status.DONE.getMessage());
        sprintTaskEntity.setStoryPoints(2);
        sprintTaskEntity.setPriority(Priority.LOW.getMessage());
        sprintTaskEntity.setComments(new ArrayList<>());
        sprintTaskEntity = sprintTaskRepository.save(sprintTaskEntity);

        SprintTaskEntity sprintTaskEntity2 = new SprintTaskEntity();
        sprintTaskEntity2.setSprint(sprintEntity);
        sprintTaskEntity2.setKey(backlogEntity.getKey() + "-T-15");
        sprintTaskEntity2.setId(2);
        sprintTaskEntity2.setTitle("dsa title");
        sprintTaskEntity2.setDescription("sadasd dfs");
        sprintTaskEntity2.setEndDate(new GregorianCalendar(2018, 5, 4).getTime());
        sprintTaskEntity2.setStatus(Status.DONE.getMessage());
        sprintTaskEntity2.setStoryPoints(2);
        sprintTaskEntity2.setPriority(Priority.LOW.getMessage());
        sprintTaskEntity2.setComments(new ArrayList<>());
        sprintTaskEntity2 = sprintTaskRepository.save(sprintTaskEntity2);*//*

//
//        sprintEntity.setTasks(sprintTaskRepository.findAllBySprintKey(sprintEntity.getKey()));
//        sprintRepository.save(sprintEntity);

//
    }
}
*/
