package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewDto {
    private String category;
    private BoardUserDto user;
    private String message;
    private SprintDto sprint;
}
