package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DashboardDto {
    TaskDto taskDto;
    ProjectDto projectDto;
    BoardDto boardDto;
    BacklogDto backlogDto;
    SprintDto sprintDto;
}
