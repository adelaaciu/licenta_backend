package dto;

import lombok.Data;

import java.util.List;

@Data
public class ProjectDto {

    private String name;
    private String description;
    private List<ProjectUserDto> users;
    private List<BoardDto> boards;
}
