package dto;

import lombok.Data;

@Data
public class BacklogTaskDto extends TaskDto{
    private BacklogDto backlog;
}
