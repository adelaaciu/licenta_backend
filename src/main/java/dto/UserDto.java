package dto;

import lombok.Data;

@Data
public class UserDto {
    private String role;
    private String mail;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String birthday;
}
