package dto;

import lombok.Data;

@Data
public class CommentDto {
    private  String key;
    private String description;
    private String userEmail;
    private String taskKey;
}
