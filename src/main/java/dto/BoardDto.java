package dto;

import lombok.Data;

import java.util.List;

@Data
public class BoardDto {
    private String key;
    private String name;
    private BacklogDto backlog;
    private ProjectDto project;
    private List<SprintDto> sprints;
    private List<BoardUserDto> boardUsers;
}
