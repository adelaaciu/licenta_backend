package dto;

import lombok.Data;

import java.util.List;

@Data
public class BoardUserDto {
    private ProjectUserDto user;
    private BoardDto board;
}
