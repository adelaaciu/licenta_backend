package dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class TaskDto {
    private String key;
    private String title;
    private String description;
    private String status;
    private BoardUserDto watcher;
    private BoardUserDto assignee;
    private Date startDate;
    private Date endDate;
    private Date actualizationDate;
    private List<CommentDto> comments;
    private int storyPoints;
    private String priority;
}
