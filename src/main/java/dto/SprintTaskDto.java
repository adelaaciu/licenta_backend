package dto;

import lombok.Data;

@Data
public class SprintTaskDto extends TaskDto{
    private SprintDto sprint;
}
