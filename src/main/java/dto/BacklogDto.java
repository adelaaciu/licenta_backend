package dto;

import lombok.Data;

import java.util.List;

@Data
public class BacklogDto {
    private String key;
    private List<BacklogTaskDto> tasks;
//    private BoardDto board;
}
