package dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class SprintDto {
    private String key;
    private int sprintNo;
    private BoardDto board;
    private List<SprintTaskDto> tasks;
    private Date startDate;
    private Date endDate;
    private boolean closed;
    private int days;
}
