package dto;

import lombok.Data;

@Data
public class ProjectUserDto {
    private ProjectDto project;
    private UserDto user;
    private String projectRole;
}
