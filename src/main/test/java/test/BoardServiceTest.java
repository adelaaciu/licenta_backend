package test;

import dto.BoardDto;
import model.BoardEntity;
import model.ProjectEntity;
import model.ProjectUserEntity;
import model.UserEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import repository.*;
import service.api.BoardService;

import java.util.ArrayList;
import java.util.List;

public class BoardServiceTest extends AbstractServiceTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProjectUserRepository projectUserRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private BoardRepository boardRepository;
    @Autowired
    private BoardService boardService;
    private String projectName = "WBC";
    @Autowired
    private BoardUserRepository boardUserRepository;
    private ProjectUserEntity projectUserEntity1;
    private ProjectEntity projectEntity1;

    @Before
    public void init() {
        UserEntity userEntity1 = new UserEntity();
        userEntity1.setRole("admin");
        userEntity1.setMail("admin");
        userEntity1.setPassword("randomGen123*!");
        userEntity1.setFirstName("Maria");
        userEntity1.setLastName("Popescu");
        userEntity1.setPhoneNumber("0774856129");
        userEntity1 = userRepository.save(userEntity1);

        UserEntity userEntity2 = new UserEntity();
        userEntity2.setRole("productOwner");
        userEntity2.setMail("test");
        userEntity2.setPassword("randomGen123*!");
        userEntity2.setFirstName("Alexa");
        userEntity2.setLastName("Alexandrescu");
        userEntity2.setPhoneNumber("0774856349");
        userEntity2 = userRepository.save(userEntity2);

        projectEntity1 = new ProjectEntity();
        projectEntity1.setName(projectName);
        projectEntity1.setBoards(new ArrayList<>());
        projectEntity1.setUsers(new ArrayList<>());
        projectEntity1.setDescription("Proiectul consta in realizarea unei aplicatii descktop in cadrul companiei X, care sa fie folosita de catre secretare. Detaliile de implementare vor putea fii gasite pe linkul: http://thinkagle.wbc.implementarion.details.com");
        projectEntity1 = projectRepository.save(projectEntity1);

        projectUserEntity1 = new ProjectUserEntity();
        projectUserEntity1.setUser(userEntity1);
        projectUserEntity1.setProject(projectEntity1);
        projectUserEntity1 = projectUserRepository.save(projectUserEntity1);

        List<ProjectUserEntity> projectUserEntities = new ArrayList<>();
        projectUserEntities.add(projectUserEntity1);

        projectEntity1.setUsers(projectUserEntities);
        projectEntity1 = projectRepository.save(projectEntity1);

        BoardEntity boardEntity1 = new BoardEntity();
        boardEntity1.setKey(projectEntity1.getName() + "-B-V1");
        boardEntity1.setName("Backlog-V1 - DB impl");
        boardEntity1.setProject(projectEntity1);
        boardEntity1.setSprints(new ArrayList<>());
        boardEntity1.setBoardUsers(new ArrayList<>());
        boardEntity1 = boardRepository.save(boardEntity1);

        BoardEntity boardEntity2 = new BoardEntity();
        boardEntity2.setKey(projectEntity1.getName() + "-B-V2");
        boardEntity2.setName("Backlog-V2 - UI design");
        boardEntity2.setProject(projectEntity1);
        boardEntity2.setSprints(new ArrayList<>());
        boardEntity2.setBoardUsers(new ArrayList<>());
        boardEntity2 = boardRepository.save(boardEntity2);

        List<BoardEntity> boards = new ArrayList<>();
        boards.add(boardEntity1);
        boards.add(boardEntity2);
        projectEntity1.setBoards(boards);
        projectEntity1 = projectRepository.save(projectEntity1);
    }


    @Test
    public void getProjectBoards() {
        List<BoardDto> projectBoards = boardService.getProjectBoards(projectName);
        Assert.assertEquals(2, projectBoards.size());
    }

    @Test
    public void getBoard() {
        BoardDto board = boardService.getBoard(projectName + "-B-V1");
        Assert.assertNotNull(board);
    }


    @Test
    public void createBoard() {
        BoardDto dto = new BoardDto();
        dto.setKey(projectName + "-B-V3");
        dto.setName("Backlog-V1 - DB impl");
        dto.setProject(projectEntity1.toDto());
        dto.setSprints(new ArrayList<>());
        dto.setBoardUsers(new ArrayList<>());

        BoardDto board = boardService.createBoard(dto);
        Assert.assertNotNull(board);
    }

    @Test
    public void deleteProjectBoard() {
        boardService.deleteProjectBoard(projectName);
        List<BoardDto> projectBoards = boardService.getProjectBoards(projectName);
        Assert.assertEquals(new ArrayList<>(), projectBoards);
    }
}
