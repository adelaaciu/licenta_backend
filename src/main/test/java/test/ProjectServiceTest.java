package test;

import dto.ProjectDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import repository.ProjectRepository;
import service.api.ProjectService;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest extends  AbstractServiceTest{
    @Autowired
    ProjectService projectService;
    @Autowired
    ProjectRepository projectRepository;

    @Before
    public void delete(){
        projectRepository.deleteAll();
    }

    @Test
    public void createProjectSuccessful() throws Exception {
        String projectName = "project";
        ProjectDto projectDTO = new ProjectDto();
        projectDTO.setDescription("description");
        projectDTO.setName(projectName);

        ProjectDto project = projectService.createProject(projectDTO);

        Assert.assertNotNull(project);
    }
    @Test
    public void getAllProjects()throws Exception{
        ProjectDto projectEntity1 = new ProjectDto();
        projectEntity1.setName("WBC");
        projectEntity1.setDescription("Proiectul consta in realizarea unei aplicatii descktop in cadrul companiei X, care sa fie folosita de catre secretare. Detaliile de implementare vor putea fii gasite pe linkul: http://thinkagle.wbc.implementarion.details.com");
        projectEntity1 =projectService.createProject(projectEntity1);


        ProjectDto projectEntity2 = new ProjectDto();
        projectEntity2.setName("Do it");
        projectEntity2.setDescription("Aplicatie pentru sportivii de performanta pentru monitorizarea starii lor de sanatate");
        projectEntity2 = projectService.createProject(projectEntity2);

        List<ProjectDto> allProjects = projectService.getAllProjects();

        Assert.assertNotNull(allProjects);

    }
    @Test
    public void getProjectsByName() throws Exception{
        ProjectDto projectEntity1 = new ProjectDto();
        projectEntity1.setName("WBC");
        projectEntity1.setDescription("Proiectul consta in realizarea unei aplicatii descktop in cadrul companiei X, care sa fie folosita de catre secretare. Detaliile de implementare vor putea fii gasite pe linkul: http://thinkagle.wbc.implementarion.details.com");
        projectService.createProject(projectEntity1);
        List<ProjectDto> wbc = projectService.getProjectsByNameContaining("WBC");

        Assert.assertNotNull(wbc);

    }

    @Test
    public void deleteProject() throws Exception {
        String projectName = "project";
        ProjectDto projectDTO = new ProjectDto();
        projectDTO.setDescription("description");
        projectDTO.setName(projectName);
        ProjectDto project = projectService.createProject(projectDTO);

        projectService.deleteProject(projectName);
        List<ProjectDto> allProjects = projectService.getAllProjects();

        Assert.assertEquals(new ArrayList<>(), allProjects);
    }
}
