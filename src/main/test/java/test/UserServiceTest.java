package test;

import dto.UserDto;
import model.UserEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import repository.UserRepository;
import service.api.UserService;

import java.util.List;

public class UserServiceTest extends AbstractServiceTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;


    @Before
    public void before() {
        userRepository.deleteAll();

        UserEntity userEntity1 = new UserEntity();
        userEntity1.setRole("admin");
        userEntity1.setMail("admin");
        userEntity1.setPassword("randomGen123*!");
        userEntity1.setFirstName("Maria");
        userEntity1.setLastName("Popescu");
        userEntity1.setPhoneNumber("0774856129");
        userEntity1 = userRepository.save(userEntity1);

        UserEntity userEntity2 = new UserEntity();
        userEntity2.setRole("productOwner");
        userEntity2.setMail("test");
        userEntity2.setPassword("randomGen123*!");
        userEntity2.setFirstName("Alexa");
        userEntity2.setLastName("Alexandrescu");
        userEntity2.setPhoneNumber("0774856349");
        userEntity2 = userRepository.save(userEntity2);
    }

    @Test
    public void getAllUsers() {
        List<UserDto> allUsers = userService.getAllUsers();
        allUsers.forEach(x->{
//            System.out.println(x.toString());
        });

        Assert.assertNotNull(allUsers);
    }

    @Test
    public void getUsersByName() {
        List<UserDto> alexa = userService.getUsersByName("Alexa");
        Assert.assertNotNull(alexa);
    }


    @Test
    public void createUser() {
        UserDto dto = new UserDto();
        dto.setRole("sadasd");
        dto.setMail("sadads");
        dto.setFirstName("Maria");
        dto.setLastName("Popescu");
        dto.setPhoneNumber("0774856129");
        dto = userService.createUser(dto);

        Assert.assertNotNull(dto);
    }

    @Test
    public void updateUser() {
        UserDto dto = new UserDto();
        dto.setRole("user");
        dto.setFirstName("Maria");
        dto.setLastName("Popescu");
        dto.setPhoneNumber("0774856129");
        dto.setMail("test");
        UserDto result = userService.updateUser(dto);

        Assert.assertEquals(dto, result);
    }


    @Test
    public void deleteUserByEmail() {
        userService.deleteUserByEmail("admin");
        Assert.assertEquals(1, userService.getAllUsers().size());
    }
}
