package test;

import dto.ProjectUserDto;
import model.ProjectEntity;
import model.ProjectUserEntity;
import model.UserEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import repository.ProjectRepository;
import repository.ProjectUserRepository;
import repository.UserRepository;
import service.api.ProjectUserService;

import java.util.ArrayList;
import java.util.List;

public class ProjectUserServiceTest extends AbstractServiceTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ProjectUserRepository projectUserRepository;
    @Autowired
    private ProjectUserService projectUserService;
    private String projectName = "WBC";
    private String userMail = "test";

    @Before
    public void init() {
        projectRepository.deleteAll();
        projectUserRepository.deleteAll();
        userRepository.deleteAll();

        UserEntity userEntity1 = new UserEntity();
        userEntity1.setRole("admin");
        userEntity1.setMail("admin");
        userEntity1.setPassword("randomGen123*!");
        userEntity1.setFirstName("Maria");
        userEntity1.setLastName("Popescu");
        userEntity1.setPhoneNumber("0774856129");
        userEntity1 = userRepository.save(userEntity1);

        UserEntity userEntity2 = new UserEntity();
        userEntity2.setRole("productOwner");
        userEntity2.setMail(userMail);
        userEntity2.setPassword("randomGen123*!");
        userEntity2.setFirstName("Alexa");
        userEntity2.setLastName("Alexandrescu");
        userEntity2.setPhoneNumber("0774856349");
        userEntity2 = userRepository.save(userEntity2);

        ProjectEntity projectEntity1 = new ProjectEntity();
        projectEntity1.setName(projectName);
        projectEntity1.setDescription("Proiectul consta in realizarea unei aplicatii descktop in cadrul companiei X, care sa fie folosita de catre secretare. Detaliile de implementare vor putea fii gasite pe linkul: http://thinkagle.wbc.implementarion.details.com");
        projectEntity1.setBoards(new ArrayList<>());
        projectEntity1.setUsers(new ArrayList<>());
        projectEntity1 = projectRepository.save(projectEntity1);


        ProjectUserEntity projectUserEntity1 = new ProjectUserEntity();
        projectUserEntity1.setUser(userEntity2);
        projectUserEntity1.setProject(projectEntity1);
        projectUserEntity1 = projectUserRepository.save(projectUserEntity1);

        ProjectUserEntity projectUserEntity2 = new ProjectUserEntity();
        projectUserEntity2.setUser(userEntity2);
        projectUserEntity2.setProject(projectEntity1);
        projectUserEntity2 = projectUserRepository.save(projectUserEntity2);


        List<ProjectUserEntity> projectUserEntities = new ArrayList<>();
        projectUserEntities.add(projectUserEntity1);
        projectUserEntities.add(projectUserEntity2);

        projectEntity1.setUsers(projectUserEntities);
        projectRepository.save(projectEntity1);
    }


    @Test
    public void getProjectUsers() {
        List<ProjectUserDto> projectUsers = projectUserService.getProjectUsers(projectName);
        Assert.assertNotNull(projectUsers);
    }

    @Test
    public void updateProjectUsers() {
        UserEntity userEntity = new UserEntity();
        userEntity.setRole("user");
        userEntity.setMail("asdas");
        userEntity.setPassword("randomGen123*!");
        userEntity.setFirstName("Alexa");
        userEntity.setLastName("Alexandrescu");
        userEntity.setPhoneNumber("0774856349");
        userRepository.save(userEntity);

        List<String> mailList = new ArrayList<>();
        mailList.add(userEntity.getMail());

        List<ProjectUserDto> projectUserDtos = projectUserService.updateProjectUsers(projectName, mailList);
        List<ProjectUserDto> projectUsers = projectUserService.getProjectUsers(projectName);

        Assert.assertEquals(projectUserDtos, projectUsers);
    }

    @Test
    public void getAllByUserMail() {
        List<ProjectUserDto> projectUsers = projectUserService.getAllByUserMail(userMail);
        Assert.assertNotNull(projectUsers);
    }

    @Test
    public void deleteProjectUser() {
        projectUserService.deleteProjectUsers(projectName);
        List<ProjectUserDto> projectUsers = projectUserService.getProjectUsers(projectName);
        Assert.assertEquals(new ArrayList<>(), projectUsers);
    }
}
